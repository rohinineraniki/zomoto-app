import React, { createContext, useState } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";

import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import OrderCards from "./components/OrderCards/OrderCards";

import Collections from "./components/Collections/Collections";
import Cities from "./components/Cities/Cities";
import OrderFood from "./components/OrderFood/OrderFood";

import NotFound from "./components/NotFound/NotFound";
import CartHeader from "./components/CartHeader/CartHeader";
import FoodItems from "./components/FoodItems/FoodItems";

import PlaceOrder from "./components/PlaceOrder/PlaceOrder";
import Payment from "./components/Payment/Payment";
import "./App.css";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route
          path="/"
          element={
            <div className="main-container">
              <Header />
              <div className="content-container">
                <OrderCards />
                <Collections />
                <Cities />
              </div>
            </div>
          }
        />
        <Route path="/order-food-online" element={<OrderFood />} />
        <Route
          path="/order-food-online/:restaurant"
          element={
            <>
              <CartHeader />
              <FoodItems />
            </>
          }
        />
        <Route
          path="/view-cart"
          element={
            <>
              <CartHeader />
              <PlaceOrder />
            </>
          }
        />
        <Route path="/payment" element={<Payment />} />
        <Route
          path="*"
          element={
            <>
              <Header />
              <NotFound />
            </>
          }
        />
      </Routes>

      <Footer />
    </BrowserRouter>
  );
}

export default App;
