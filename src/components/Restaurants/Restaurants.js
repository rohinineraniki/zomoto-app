import React from "react";
import { connect } from "react-redux";
import "./Restaurants.css";

import { useNavigate } from "react-router-dom";

function Restaurants(props) {
  const navigate = useNavigate();
  const renderRestaurant = (each) => {
    const {
      title,
      foodCategory,
      rating,
      deliveryTime,
      price,
      imageUrl,
      offerStatus,
      offer,
      couponCode,
      address,
    } = each;
    const titlePath = title.toLowerCase().split(" ").join("-");

    return (
      <div
        className="col-12 col-md-4 col-lg-4 text-white restaurants-container"
        onClick={() => navigate(`/order-food-online/${titlePath}`)}
      >
        <div className="m-2">
          <div
            className="restaurants-image d-flex align-self-end justify-content-between pe-2 mb-2"
            style={{ backgroundImage: `url(${imageUrl})` }}
          >
            <div className="offer px-1 align-self-end mb-2">
              <p>{offer}% OFF</p>
            </div>
            <div className="time-container align-self-end mb-2">
              <p>{deliveryTime} min</p>
            </div>
          </div>
          <div className="text-black mt-2 description-container">
            <div className="restaurant-title-container d-flex justify-content-between">
              <p className="title">{title}</p>

              <div className="rate-container d-flex align-items-center gap-1 px-1 pt-0 pb-0">
                <i
                  class="fa-sharp fa-solid fa-star fa-xs mb-1"
                  style={{ color: "#ffffff" }}
                ></i>
                <p>{rating}</p>
              </div>
            </div>
            <div className="d-flex justify-content-between mt-2">
              <p className="food-category">{foodCategory}</p>
              <p className="price">{price} per one</p>
            </div>
            <hr />
            <div className="order-count-container text-black d-flex justify-content-between">
              <div className="arrow-trend me-2 pt-1">
                <i
                  class="fa-solid fa-arrow-trend-up fa-xs"
                  style={{ color: "#ffffff" }}
                ></i>
              </div>
              <p className="order-count-para">
                {(rating * 100).toFixed(0)}+ orders places from here recently
              </p>
            </div>
          </div>
        </div>
      </div>
    );
  };

  return (
    <div className="container-fluid d-flex justify-content-center align-items-center ">
      <div className="row all-restaurants-container d-flex justify-content-center align-items-center ">
        <h2 className="mt-4 mb-2">
          Order Online from your favourite restaurant
        </h2>
        {props.restaurantsList.map((each) => {
          return renderRestaurant(each);
        })}
      </div>
    </div>
  );
}

export default Restaurants;
