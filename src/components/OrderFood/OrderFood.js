import React, { createContext, useEffect, useState } from "react";
import HeaderOrderFood from "../HeaderOrderFood/HeaderOrderFood";
import ShowCategories from "../ShowCategories/ShowCategories";

import Restaurants from "../Restaurants/Restaurants";
import Filters from "../Filters/Filters";
import { connect, useDispatch, useSelector } from "react-redux";

import { useLocation, useSearchParams } from "react-router-dom";
import {
  COST_LOW_TO_HIGH,
  RATING_RANGE,
  RATING_HIGH_TO_LOW,
  DELIVERY_TIME,
  COST_HIGH_TO_LOW,
  RESET,
  FILTER_CUISIENS,
  SEARCH_LIST,
} from "../../redux/reducers/actionTypes";

export const SearchRestaurant = createContext();

function OrderFood(props) {
  const [searchValue, setSearchValue] = useState("");

  const dispatch = useDispatch();
  const { search } = useLocation();

  const params = new URLSearchParams(search);

  const sortCostAce = params.getAll("sort");
  const ratingRange = params.get("rating_range");

  const existCuisine = params.get("cuisines");
  const existSort = params.get("sort");

  let getAllCuisinesFromUrl = params.getAll("cuisines");

  useEffect(() => {
    const allSortKeys = params.getAll("sort");

    if (
      ratingRange === null ||
      searchValue === "" ||
      existCuisine === null ||
      existSort === null
    ) {
      dispatch({
        type: RESET,
      });
    }

    if (searchValue !== "") {
      console.log("searchValue", searchValue);
      dispatch({
        type: SEARCH_LIST,
        payload: { searchValue },
      });
    }

    console.log("cost ace", applyFilterList);

    if (allSortKeys.includes("ca")) {
      dispatch({
        type: COST_LOW_TO_HIGH,
      });
    } else if (allSortKeys.includes("ta")) {
      dispatch({
        type: DELIVERY_TIME,
      });
    } else if (allSortKeys.includes("cd")) {
      dispatch({
        type: COST_HIGH_TO_LOW,
      });
    } else if (allSortKeys.includes("ra")) {
      dispatch({
        type: RATING_HIGH_TO_LOW,
      });
    }

    if (params.has("cuisines")) {
      dispatch({
        type: FILTER_CUISIENS,
        payload: { getAllCuisinesFromUrl },
      });
    }

    if (params.has("rating_range")) {
      let ratingValue = ratingRange.substring(0, 1);
      dispatch({
        type: RATING_RANGE,
        payload: { ratingValue },
      });
    }
  }, [
    searchValue,
    ratingRange,

    sortCostAce.includes("ca"),
    sortCostAce.includes("ta"),
    sortCostAce.includes("ra"),
    sortCostAce.includes("cd"),

    search.toString(),

    search === "",
  ]);

  const applyFilterList = useSelector((state) => {
    return state.categories.applyFilterList;
  });

  return (
    <div>
      <SearchRestaurant.Provider value={{ searchValue, setSearchValue }}>
        <HeaderOrderFood />
        <Filters />
        {search === "" && searchValue === "" && <ShowCategories />}

        {applyFilterList.length !== 0 ? (
          <>
            {console.log("at render list", applyFilterList)}
            <Restaurants restaurantsList={applyFilterList} />
          </>
        ) : (
          <div className="d-flex flex-column align-items-center vh-100">
            <h2>Oops!</h2>
            <p>
              We could not understand what you mean, try rephrasing the query
            </p>
          </div>
        )}
      </SearchRestaurant.Provider>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    restaurantsList: state.categories.restaurants,

    applyFilterList: state.categories.applyFilterList,
  };
};

export default connect(mapStateToProps)(OrderFood);
