import React from "react";
import notfoundImage from "../../images/notfound-image.avif";
import { Link } from "react-router-dom";

import "./NotFound.css";

function NotFound() {
  return (
    <div className="d-flex justify-content-center ">
      <div className="not-found-container d-flex flex-column flex-md-row align-items-center ">
        <div>
          <img
            src={notfoundImage}
            alt="Not-found"
            className="not-found-image"
          />
        </div>
        <div className="text-center">
          <p>
            This is a 404 page and we think it's fairly clear You aren't going
            to find what you're looking for here But we know you're hungry, so
            don't fret or rage Hit that big red button to go back to our
            homepage
          </p>
          <Link to="/">
            <button className="btn btn-danger">Back to home</button>
          </Link>
        </div>
      </div>
    </div>
  );
}

export default NotFound;
