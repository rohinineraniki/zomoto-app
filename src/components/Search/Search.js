import React from "react";
import "./Search.css";

function Search() {
  return (
    <div className="container-fluid d-flex align-tems-center justify-content-center">
      <div className="row search-container d-flex flex-sm-column flex-md-row ps-2 m-3 d-md-none">
        <div className="col-12 col-md-4 d-flex align-items-center gap-2 bg-white search-bg">
          <i
            class="fa-solid fa-location-dot fa-lg"
            style={{ color: "#e85f5f" }}
          ></i>
          <input type="text" placeholder="Location" className="text-input" />
          <i class="fa-solid fa-caret-down" style={{ color: "#384251" }}></i>
          <hr className="line align-self-end d-none d-md-inline" />
        </div>
        <div className="col-12 col-md-6 d-flex align-items-center gap-2 mt-3 bg-white search-bg">
          <i
            class="fa-solid fa-magnifying-glass"
            style={{ color: "#384251" }}
          ></i>
          <input
            type="text"
            placeholder="Search for restaurant, cuisine or a dish"
            className="text-input"
          />
        </div>
      </div>
      <div className="row search-container flex-md-row ps-2 m-1 d-none d-md-flex bg-white m-3 w-50">
        <div className="col-md-4 d-flex align-items-center gap-2 ">
          <i
            class="fa-solid fa-location-dot fa-lg"
            style={{ color: "#e85f5f" }}
          ></i>
          <input type="text" placeholder="Location" className="text-input" />
          <i class="fa-solid fa-caret-down" style={{ color: "#384251" }}></i>
          <hr className="line align-self-end d-none d-md-inline" />
        </div>
        <div className="col-md-6 d-flex align-items-center gap-2">
          <i
            class="fa-solid fa-magnifying-glass"
            style={{ color: "#384251" }}
          ></i>
          <input
            type="text"
            placeholder="Search for restaurant, cuisine or a dish"
            className="text-input"
          />
        </div>
      </div>
    </div>
  );
}

export default Search;
