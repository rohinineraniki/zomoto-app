import React from "react";
import { useNavigate } from "react-router-dom";
import "./HomeCard.css";

function HomeCard(props) {
  const navigate = useNavigate();
  const { orderimage, text, textPara, urlPath } = props;
  console.log(orderimage);
  return (
    <div className="col-6 col-lg-4 card-container mb-3">
      <div
        className="card each-card"
        onClick={() => {
          navigate(`${urlPath}`);
        }}
      >
        <img className="card-img-top order-image" src={orderimage} alt={text} />
        <div className="card-body">
          <h6 className="card-title">{text}</h6>
          <p className="card-text">{textPara}</p>
        </div>
      </div>
    </div>
  );
}

export default HomeCard;
