import React, { useEffect, useState } from "react";
import {
  Link,
  useLocation,
  useSearchParams,
  useNavigate,
} from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import Cuisines from "../Cuisines/Cuisines";

import "./Filters.css";

function Filters(props) {
  const [filterPopup, setFilterPopup] = useState(false);
  const dispatch = useDispatch();

  const navigate = useNavigate();
  const { search, pathname } = useLocation();
  const searchUrlParams = new URLSearchParams(search);

  const getCusiniesFromUrl = searchUrlParams.getAll("cuisines");

  const [appliedFilterTypes, setAppliedFilterTypes] = useState({
    sortType: "popularity",
    checkedCuisinesList: getCusiniesFromUrl,
    ratingClick: 0,
    ratingMarkId: "1",
  });

  const [activeDiv, setActiveDiv] = useState(1);

  const handleRating = () => {
    searchUrlParams.append("rating_range", "4.0-5.0");
    setAppliedFilterTypes({ ...appliedFilterTypes, ratingClick: 0 });

    navigate({
      pathname: pathname,
      search: searchUrlParams.toString(),
    });
  };

  const handleSortRemove = () => {
    searchUrlParams.delete("sort");
    setAppliedFilterTypes({ ...appliedFilterTypes, sortType: "popularity" });

    navigate({
      pathname: pathname,
      search: searchUrlParams.toString(),
    });
  };

  const handleRatingRemove = () => {
    searchUrlParams.delete("rating_range");
    setAppliedFilterTypes({ ...appliedFilterTypes, ratingClick: 0 });

    navigate({
      pathname: pathname,
      search: searchUrlParams.toString(),
    });
  };

  const handleFiltersClick = () => {
    setFilterPopup(true);
  };

  const handleFiltersRemove = () => {
    setFilterPopup(false);
  };

  const handleFilterSortBy = (id) => {
    setActiveDiv(1);
  };

  const handleFilterCuisines = () => {
    setActiveDiv(2);
  };

  const handleFilterRating = () => {
    setActiveDiv(3);
  };

  const handleFilterCostPerPerson = () => {
    setActiveDiv(4);
  };

  const handleFilterMoreFilters = () => {
    setActiveDiv(5);
  };

  const handleRatingMark = (event) => {
    console.log("rating mark", event.target.id);
    setAppliedFilterTypes({
      ...appliedFilterTypes,
      ratingMarkId: event.target.id,
    });
  };

  const handleEachSortFilter = (event) => {
    setAppliedFilterTypes({ ...appliedFilterTypes, sortType: event.target.id });
  };

  const getCuisinesList = (addCuisine, removeCuisine) => {
    if (addCuisine !== null) {
      setAppliedFilterTypes({
        ...appliedFilterTypes,
        checkedCuisinesList: [
          ...appliedFilterTypes.checkedCuisinesList,
          addCuisine,
        ],
      });
    } else if (removeCuisine !== null) {
      let removedCuisinesList = appliedFilterTypes.checkedCuisinesList.filter(
        (each) => {
          return each !== removeCuisine;
        }
      );

      setAppliedFilterTypes({
        ...appliedFilterTypes,
        checkedCuisinesList: removedCuisinesList,
      });
    }
  };

  const handleRemoveAllFilters = () => {
    navigate("/order-food-online");

    setFilterPopup(false);
  };

  let ratingString;

  const handleApplyAllFilters = () => {
    switch (appliedFilterTypes.sortType) {
      case "ratingAse":
        searchUrlParams.set("sort", "ra");

        navigate({
          pathname: pathname,
          search: searchUrlParams.toString(),
        });

        break;
      case "deliveryTime":
        searchUrlParams.set("sort", "ta");

        navigate({
          pathname: pathname,
          search: searchUrlParams.toString(),
        });
        break;
      case "costLowToHigh":
        searchUrlParams.set("sort", "ca");

        navigate({
          pathname: pathname,
          search: searchUrlParams.toString(),
        });
        break;
      case "costHighToLow":
        searchUrlParams.set("sort", "cd");

        navigate({
          pathname: pathname,
          search: searchUrlParams.toString(),
        });
        break;
      case "popularity":
        searchUrlParams.delete("sort");

        navigate({
          pathname: pathname,
          search: searchUrlParams.toString(),
        });
        break;
      default:
        console.log("in default", appliedFilterTypes.sortType === "ratingAse");
        navigate({
          pathname: pathname,
          search: "",
        });
    }

    if (appliedFilterTypes.checkedCuisinesList.length !== 0) {
      const setCuisinesPath = appliedFilterTypes.checkedCuisinesList.map(
        (each, index) => {
          if (index === 0) {
            searchUrlParams.set("cuisines", each);
          } else {
            searchUrlParams.append("cuisines", each);
          }
          return searchUrlParams.toString();
        }
      );

      navigate({
        pathname: pathname,
        search: setCuisinesPath.pop(),
      });
    } else if (appliedFilterTypes.checkedCuisinesList.length === 0) {
      searchUrlParams.delete("cuisines");

      navigate({
        pathname: pathname,
        search: searchUrlParams.toString(),
      });
    }

    activeDiv === 3 &&
      (() => {
        ratingString =
          appliedFilterTypes.ratingMarkId + "." + "0" + "-" + "5.0";
        searchUrlParams.set("rating_range", ratingString);

        navigate({
          pathname: pathname,
          search: searchUrlParams.toString(),
        });
      })();

    setFilterPopup(false);
  };

  return (
    <>
      <div className="filter-container ">
        <div className="bg-light d-flex gap-3 flex-wrap ms-5 mb-5 p-4 ">
          <div
            className="each-filter-container d-inline-flex gap-2 p-2"
            onClick={handleFiltersClick}
          >
            <i className="fa-solid fa-filter" style={{ color: "#bbbec3" }}></i>
            <p className="filter-text">Filters</p>
          </div>

          {searchUrlParams.getAll("sort").includes("ra") && (
            <div className="applied-filter-container d-inline-flex gap-2 p-2">
              <p>Rating: High to Low</p>
              <i
                class="fa-solid fa-xmark"
                style={{ color: "#f5f7fa" }}
                onClick={handleSortRemove}
              ></i>
            </div>
          )}

          {searchUrlParams.getAll("sort").includes("ta") && (
            <div className="applied-filter-container d-inline-flex gap-2 p-2">
              <p>Delivery Time</p>
              <i
                class="fa-solid fa-xmark"
                style={{ color: "#f5f7fa" }}
                onClick={handleSortRemove}
              ></i>
            </div>
          )}

          {searchUrlParams.getAll("sort").includes("ca") && (
            <div className="applied-filter-container d-inline-flex gap-2 p-2">
              <p>Cost: Low to High</p>
              <i
                className="fa-solid fa-xmark"
                style={{ color: "#ffffff" }}
                onClick={handleSortRemove}
              ></i>
            </div>
          )}

          {searchUrlParams.getAll("sort").includes("cd") && (
            <div className="applied-filter-container d-inline-flex gap-2 p-2">
              <p>Cost: High to Low</p>
              <i
                className="fa-solid fa-xmark"
                style={{ color: "#ffffff" }}
                onClick={handleSortRemove}
              ></i>
            </div>
          )}

          {searchUrlParams.has("rating_range") ? (
            <div className="applied-filter-container d-inline-flex gap-2 p-2">
              <p>Rating: {searchUrlParams.get("rating_range")[0]}.0+</p>
              <i
                class="fa-solid fa-xmark"
                style={{ color: "#f5f7fa" }}
                onClick={handleRatingRemove}
              ></i>
            </div>
          ) : (
            <div className="each-filter-container d-inline-flex gap-2 p-2">
              <p className="filter-text" onClick={handleRating}>
                Rating: 4.0+
              </p>
            </div>
          )}

          <div className="each-filter-container d-inline-flex gap-2 p-2">
            <p className="filter-text">Cuisines</p>
          </div>
          <div className="each-filter-container d-inline-flex gap-2 p-2">
            <p className="filter-text">More Filters</p>
          </div>
        </div>
      </div>
      {filterPopup && (
        <div className="bg-blur">
          <div className="filter-popup-container">
            <div className="d-flex justify-content-between align-items-center  ">
              <h4>Filters</h4>
              <i
                className="fa-solid fa-xmark icon"
                style={{ color: "#000000" }}
                onClick={handleFiltersRemove}
              ></i>
            </div>
            <div className="all-filters-container row">
              <div className="col-4 ">
                <div className="filter-sidebar-container d-flex flex-column  pt-4 pb-4 w-100 m-0">
                  <div
                    className={
                      activeDiv === 1 ? "active-div" : "ps-3 inactive-div"
                    }
                    onClick={() => {
                      handleFilterSortBy();
                    }}
                  >
                    <p>Sort by</p>
                  </div>
                  <div
                    onClick={handleFilterCuisines}
                    className={
                      activeDiv === 2 ? "active-div" : "ps-3 inactive-div"
                    }
                  >
                    <p>Cuisines</p>
                  </div>
                  <div
                    onClick={handleFilterRating}
                    className={
                      activeDiv === 3 ? "active-div" : "ps-3 inactive-div"
                    }
                  >
                    <p>Rating</p>
                  </div>
                  <div
                    onClick={handleFilterCostPerPerson}
                    className={
                      activeDiv === 4 ? "active-div" : "ps-3 inactive-div"
                    }
                  >
                    <p>Cost per person</p>
                  </div>
                  <div
                    onClick={handleFilterMoreFilters}
                    className={
                      activeDiv === 5 ? "active-div" : "ps-3 inactive-div"
                    }
                  >
                    <p>More filters</p>
                  </div>
                </div>
              </div>
              <div className="filter-select-container col-7">
                {activeDiv === 1 && (
                  <div className="d-flex flex-column gap-3 mt-3">
                    <div className="form-check">
                      <input
                        className="form-check-input"
                        type="radio"
                        name="sortby"
                        id="popularity"
                        defaultChecked="checked"
                        checked={appliedFilterTypes.sortType === "popularity"}
                        onChange={handleEachSortFilter}
                      />
                      <label className="form-check-label mt-1" for="popularity">
                        Popularity
                      </label>
                    </div>
                    <div className="form-check">
                      <input
                        className="form-check-input"
                        type="radio"
                        name="sortby"
                        id="ratingAse"
                        checked={appliedFilterTypes.sortType === "ratingAse"}
                        onChange={handleEachSortFilter}
                      />
                      <label className="form-check-label" for="ratingAse">
                        Rating: High to Low
                      </label>
                    </div>
                    <div className="form-check">
                      <input
                        className="form-check-input"
                        type="radio"
                        name="sortby"
                        id="deliveryTime"
                        checked={appliedFilterTypes.sortType === "deliveryTime"}
                        onChange={handleEachSortFilter}
                      />
                      <label className="form-check-label" for="deliveryTime">
                        Delivery Time
                      </label>
                    </div>
                    <div className="form-check">
                      <input
                        className="form-check-input"
                        type="radio"
                        name="sortby"
                        id="costLowToHigh"
                        checked={
                          appliedFilterTypes.sortType === "costLowToHigh"
                        }
                        onChange={handleEachSortFilter}
                      />
                      <label className="form-check-label" for="costLowToHigh">
                        Cost: Low to High
                      </label>
                    </div>
                    <div className="form-check">
                      <input
                        className="form-check-input"
                        type="radio"
                        name="sortby"
                        id="costHighToLow"
                        checked={
                          appliedFilterTypes.sortType === "costHighToLow"
                        }
                        onChange={handleEachSortFilter}
                      />
                      <label className="form-check-label" for="costHighToLow">
                        Cost: High to Low
                      </label>
                    </div>
                  </div>
                )}

                {activeDiv === 2 && (
                  <Cuisines
                    getCuisinesList={getCuisinesList}
                    prevCuisinesList={appliedFilterTypes.checkedCuisinesList}
                  />
                )}

                {activeDiv === 3 && (
                  <>
                    <div>
                      <div className="mt-3">
                        <h6 className="text-black">Rating</h6>
                        <h5>{appliedFilterTypes.ratingMarkId}.0+</h5>
                      </div>
                      <div className="rating-slider-container">
                        <span className="rating-slider"></span>

                        {appliedFilterTypes.ratingMarkId === "1" && (
                          <span className="rating-track track-1"></span>
                        )}

                        {appliedFilterTypes.ratingMarkId === "2" && (
                          <span className="rating-track track-2"></span>
                        )}
                        {appliedFilterTypes.ratingMarkId === "3" && (
                          <span className="rating-track track-3"></span>
                        )}
                        {appliedFilterTypes.ratingMarkId === "4" && (
                          <span className="rating-track track-4"></span>
                        )}

                        <span
                          className={
                            appliedFilterTypes.ratingMarkId === "1"
                              ? "rating-mark-active large-mark mark-1"
                              : "rating-mark mark-1"
                          }
                          id="1"
                          onClick={handleRatingMark}
                        ></span>

                        <span className="rating-label mark-1">any</span>

                        <span
                          className={
                            appliedFilterTypes.ratingMarkId === "1"
                              ? "rating-mark-active mark-2"
                              : appliedFilterTypes.ratingMarkId === "2"
                              ? "rating-mark-active mark-2 large-mark"
                              : "rating-mark mark-2"
                          }
                          id="2"
                          onClick={handleRatingMark}
                        ></span>
                        <span className="rating-label mark-2">2</span>

                        <span
                          className={
                            appliedFilterTypes.ratingMarkId === "1" ||
                            appliedFilterTypes.ratingMarkId === "2"
                              ? "rating-mark-active mark-3"
                              : appliedFilterTypes.ratingMarkId === "3"
                              ? "rating-mark-active large-mark mark-3"
                              : "rating-mark mark-3"
                          }
                          id="3"
                          onClick={handleRatingMark}
                        ></span>

                        <span className="rating-label mark-3">3</span>

                        <span
                          className={
                            appliedFilterTypes.ratingMarkId === "1" ||
                            appliedFilterTypes.ratingMarkId === "2" ||
                            appliedFilterTypes.ratingMarkId === "3"
                              ? "rating-mark-active mark-4"
                              : appliedFilterTypes.ratingMarkId === "4"
                              ? "rating-mark-active mark-4 large-mark"
                              : "rating-mark mark-4"
                          }
                          id="4"
                          onClick={handleRatingMark}
                        ></span>

                        <span className="rating-label mark-4">4</span>

                        <span className="rating-mark-active mark-5"></span>
                        <span className="rating-label mark-5">5</span>
                      </div>
                    </div>
                  </>
                )}
              </div>
            </div>
            <div className="d-flex justify-content-end mt-3">
              <div className="d-flex gap-2">
                <button
                  className="btn btn-secondary"
                  onClick={handleRemoveAllFilters}
                >
                  Clear all
                </button>
                <button
                  className="btn btn-danger"
                  onClick={handleApplyAllFilters}
                >
                  Apply
                </button>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
}

export default Filters;
