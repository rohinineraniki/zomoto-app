import React from "react";
import { connect } from "react-redux";

import "./ShowCategories.css";

function ShowCategories(props) {
  const renderCategories = (each) => {
    return (
      <div className="col-3 col-md-2 col-xl-1 category-container d-flex align-items-center gap-2 ms-md-5">
        <div className="category-container d-flex flex-column align-items-center gap-2">
          <img
            src={each.foodCategoryImageUrl}
            alt={each.title}
            className="category-image"
          />
          <p>{each.title}</p>
        </div>
      </div>
    );
  };

  return (
    <>
      <div className="container-fluid">
        <div className="row">
          <h4 className="category-heading">Inspiration for your first order</h4>
          {props.categoriesList.map((each) => {
            return renderCategories(each);
          })}
        </div>
      </div>
    </>
  );
}

function mapStateToProps(state) {
  return {
    categoriesList: state.categories.categories,
  };
}

export default connect(mapStateToProps)(ShowCategories);
