import React, { useState } from "react";
import { useParams, Link } from "react-router-dom";
import { connect } from "react-redux";
import EachFoodItem from "../EachFoodItem/EachFoodItem";
import "./FoodItems.css";

function FoodItems(props) {
  const { restaurant } = useParams();

  const clickedRestaurant = props.restaurantsList.filter((each) => {
    return each.title.toLowerCase() === restaurant.split("-").join(" ");
  });

  const getRestaurant = props.restaurantsList.filter((each) => {
    const filteredDishes = each.dishes.filter((eachDish) => {
      return eachDish.quantity > 0;
    });
    if (filteredDishes.length !== 0) {
      return each;
    }
  });

  // if (getRestaurant.length !== 0) {
  //   let getQuantity = getRestaurant[0].dishes.reduce((acc, each) => {
  //     acc += each.quantity;
  //     return acc;
  //   }, 0);
  //   console.log("getQuantity", getQuantity);
  //   setQuantity(getQuantity);
  // }

  const getQuantity = () => {
    if (getRestaurant.length !== 0) {
      let getQuantity = getRestaurant[0].dishes.reduce((acc, each) => {
        acc += each.quantity;
        return acc;
      }, 0);
      console.log("getQuantity", getQuantity);
      // setQuantity(getQuantity);
      return <p>{getQuantity} Items Added</p>;
    } else {
      return <p>No Items Added</p>;
    }
  };

  const {
    dishes,
    address,
    couponCode,
    deliveryTime,
    foodCategory,
    imageUrl,
    offer,
    offerStatus,
    title,
    rating,
    price,
  } = clickedRestaurant[0];

  return (
    <>
      <div className="title-container d-flex justify-content-center text-black my-4">
        <div className="d-flex flex-column title-details-container mt-2">
          <h1>{title}</h1>
          <p>{foodCategory}</p>
          <p>{address}</p>
          <p>{deliveryTime} mins to delivery </p>
          <div className="rate-container d-flex align-items-center gap-1 px-1 pt-0 pb-0">
            <i
              class="fa-sharp fa-solid fa-star fa-xs mb-1"
              style={{ color: "#ffffff" }}
            ></i>
            <p>{rating}</p>
          </div>
        </div>
      </div>
      <div className="container-fluid d-flex justify-content-center">
        <div className="row d-flex flex-column all-items-container">
          <h2 className="my-3">Food Items</h2>
          {dishes.map((each) => {
            return (
              <EachFoodItem
                foodItem={each}
                key={each.id}
                restautantTitle={title}
              />
            );
          })}
        </div>
      </div>
      {getRestaurant.length > 0 ? (
        <div className="footer-order-items-container  d-flex  justify-content-center">
          <div className="d-flex justify-content-between align-items-center gap-5 footer-order-container">
            <div>
              <i class="fa-solid fa-pot-food" style={{ color: "#414244" }}></i>
              {getQuantity()}
            </div>
            <div>
              <Link to="/view-cart">
                <button className="btn btn-danger">Next</button>
              </Link>
            </div>
          </div>
        </div>
      ) : null}
    </>
  );
}

function mapStateToProps(state) {
  return {
    restaurantsList: state.categories.restaurants,
    orderItemsList: state.categories.cartItems.orderItemsList,
  };
}

export default connect(mapStateToProps)(FoodItems);
