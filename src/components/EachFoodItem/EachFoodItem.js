import React, { useState } from "react";
import "./EachFoodItem.css";
import { connect } from "react-redux";

function EachFoodItem(props) {
  const {
    about,
    dishImage,
    dishName,
    id,
    isBestSeller,
    price,

    rating,
    reviews,
    quantity,
    isVeg,
  } = props.foodItem;
  const { restautantTitle } = props;

  const [addCart, setAddCart] = useState(false);

  const handleCart = () => {
    setAddCart(true);
    console.log("in fooditem", restautantTitle);
    props.addToCart(id, restautantTitle, props.foodItem, quantity);
  };

  const handleDecrement = () => {
    if (quantity === 1) {
      setAddCart(false);
    }
  };

  return (
    <div className="col-12 col-md-12 d-flex gap-mb-4 gap-2 mb-3 single-dish-container">
      <div className="mb-3 dish-image-container">
        <img src={dishImage} alt="dish" className="dish-image mt-1 me-2" />
        <div
          className={
            isVeg
              ? "veg-text-container d-flex align-items-center justify-content-center"
              : "nonveg-text-container veg-text-container d-flex align-items-center justify-content-center"
          }
        >
          {isVeg ? (
            <i
              className="fa-solid fa-circle fa-xs"
              style={{ color: "#19cc46" }}
            ></i>
          ) : (
            <i
              className="fa-sharp fa-solid fa-play fa-rotate-270 fa-xs"
              style={{ color: "#a60c4f" }}
            ></i>
          )}
        </div>
      </div>
      <div className="mb-3 dish-title-container d-flex flex-column gap-2">
        <h3>{dishName}</h3>
        <div>
          <button className="btn btn-success me-2 mb-2 cart-button">
            Must Try
          </button>
          <button className="btn btn-danger cart-button mb-2">
            Best Seller
          </button>
        </div>
        <div className="d-flex align-items-center gap-4 mt-2">
          <div className="rate-container d-flex align-items-center gap-1 px-1 pt-0 pb-0">
            <i
              class="fa-sharp fa-solid fa-star fa-xs mb-1"
              style={{ color: "#ffffff" }}
            ></i>
            <p>{rating}</p>
          </div>
          <p>{reviews} votes</p>
        </div>
        <div className="d-flex">
          <i
            class="fa-solid fa-indian-rupee-sign"
            style={{ color: "#3a3a3b" }}
          ></i>
          <p>{price}</p>
        </div>

        {addCart || quantity >= 1 ? (
          <div className="d-flex gap-4 quatity-container ">
            <p
              className="quatity-para"
              onClick={() => {
                props.decrement(id, restautantTitle);
                handleDecrement();
              }}
            >
              -
            </p>
            <p>{quantity}</p>
            <p
              className="quatity-para"
              onClick={() => {
                props.increment(id, restautantTitle);
              }}
            >
              +
            </p>
          </div>
        ) : (
          <button
            className="btn btn-warning add-cart-button"
            onClick={handleCart}
          >
            Add to Cart
          </button>
        )}
        <div>
          <p className="about-para">{about}</p>
        </div>
      </div>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    restaurantsList: state.categories.restaurants,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    increment: (id, restautantTitle) => {
      dispatch({
        type: "INCREASE_QUANTITY",
        payload: { id, restautantTitle },
      });
    },
    decrement: (id, restautantTitle) => {
      dispatch({
        type: "DECREASE_QUANTITY",
        payload: { id, restautantTitle },
      });
    },
    addToCart: (id, restautantTitle, foodItem, quantity) => {
      dispatch({
        type: "ADD_TO_CART",
        payload: { id, restautantTitle, foodItem, quantity },
      });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(EachFoodItem);
