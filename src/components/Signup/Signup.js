import React, { useState } from "react";
import validator from "validator";
import googleLogo from "../../images/header/google-logo.webp";
import { Link } from "react-router-dom";

function Signup(props) {
  const { removeSignup } = props;

  const [fullName, setFullName] = useState("");
  const [email, setEmail] = useState("");

  const [validate, isValidate] = useState(false);
  const [nameError, isNameError] = useState(false);

  const [emailError, isEmailError] = useState(false);
  const [emailErrorMsg, setEmailErrorMsg] = useState("");

  const [userDetails, setUserDetails] = useState([]);

  const handleSubmit = (event) => {
    event.preventDefault();

    const checkNewUser = localStorage.getItem("userSignup");

    if (checkNewUser !== null) {
      const checkUserExist = JSON.parse(checkNewUser).filter((each) => {
        return each.name === fullName;
      });
      console.log("checkUserExist", checkUserExist);
    }

    if (
      validator.isAlpha(fullName, "en-IN", { ignore: " " }) &&
      validator.isEmail(email)
    ) {
      console.log("validation done");

      if (checkNewUser === null) {
        setUserDetails([{ name: fullName, email: email }]);
        isValidate(true);
      } else {
        const newUserSignup = JSON.parse(checkNewUser).filter((each) => {
          return each.email === email;
        });
        console.log("check new user", newUserSignup);
        if (newUserSignup.length === 0) {
          setUserDetails([
            ...JSON.parse(localStorage.getItem("userSignup")),
            { name: fullName, email: email },
          ]);
          isValidate(true);
        } else {
          setEmailErrorMsg("Email already exists, please enter another mail");
        }
      }

      console.log("userDetails", userDetails);
    } else {
      isValidate(false);
      console.log("not validated");
    }
  };

  const handleNameBlur = (event) => {
    if (
      event.target.value === "" ||
      !validator.isAlpha(event.target.value, "en-IN", { ignore: " " })
    ) {
      isNameError(true);
    }
  };

  const handleEmailBlur = (event) => {
    if (event.target.value === "" || !validator.isEmail(event.target.value)) {
      isEmailError(true);
    }
  };

  const handleNameChange = (event) => {
    setFullName(event.target.value);
  };

  const handleEmailChange = (event) => {
    setEmail(event.target.value);
    setEmailErrorMsg("");
  };

  const handleCross = () => {
    removeSignup(false);
  };

  const handleHome = () => {
    isValidate(false);
    removeSignup(false);
    console.log("userDetails to local", userDetails);
    localStorage.setItem("userSignup", JSON.stringify(userDetails));
  };

  const handleLogin = () => {
    removeSignup(false, true);
  };

  return (
    <>
      {
        <div className="bg-blur">
          <div className="signup-form-container text-black signup-popup py-4">
            <form onSubmit={handleSubmit}>
              <div className="d-flex justify-content-between align-items-center mb-4">
                <h2>Signup</h2>
                <i
                  class="fa-solid fa-xmark fa-lg"
                  style={{ color: "#2a2b2c" }}
                  onClick={handleCross}
                ></i>
              </div>
              {console.log("userdetails", userDetails)}
              <div className="mb-4">
                <input
                  type="text"
                  className="signup-input form-control"
                  placeholder="Full Name"
                  name="fullName"
                  onBlur={handleNameBlur}
                  onChange={handleNameChange}
                  required
                />
                {nameError &&
                  !validator.isAlpha(fullName, "en-IN", { ignore: " " }) && (
                    <p className="error-msg">Please enter a valid name</p>
                  )}
              </div>
              <div className="mb-4">
                <input
                  type="email"
                  className="signup-input form-control"
                  placeholder="Email"
                  name="email"
                  onBlur={handleEmailBlur}
                  onChange={handleEmailChange}
                  required
                />
                {emailError && !validator.isEmail(email) && (
                  <p className="error-msg">Invalid Email id</p>
                )}
                {emailErrorMsg !== "" && (
                  <p className="error-msg">{emailErrorMsg}</p>
                )}
              </div>
              <div className="mb-3 d-flex">
                <input type="checkbox" id="terms" required />
                <label htmlFor="terms" className="label ms-2">
                  I agree to Zomoato's
                  <a href="#" className="terms-anchor">
                    Terms of service, Privacy Policy and Content Policies
                  </a>
                </label>
              </div>
              <button
                className={
                  validator.isAlpha(fullName, "en-IN", { ignore: " " }) &&
                  validator.isEmail(email)
                    ? "btn btn-danger text-white w-100"
                    : "btn btn-secondary text-white w-100"
                }
                type="submit"
              >
                Create account
              </button>
            </form>
            <div className="d-flex flex-column">
              <hr />
              <p className="align-self-center">or</p>
              <button className="btn btn-outline-secondary w-100">
                <img src={googleLogo} alt="logo" className="google-logo" />
                Continue with Google
              </button>
              <hr />
              <p>
                Already have an account?{" "}
                <a href="#" className="terms-anchor" onClick={handleLogin}>
                  Login
                </a>
              </p>
            </div>
          </div>
        </div>
      }
      {validate && (
        <div className="bg-blur">
          <div className="success-popup text-success d-flex flex-column align-items-center gap-2">
            <h5>Signup Successfully!!</h5>
            <Link to="/">
              <button className="btn btn-success" onClick={handleHome}>
                Home
              </button>
            </Link>
          </div>
        </div>
      )}
    </>
  );
}

export default Signup;
