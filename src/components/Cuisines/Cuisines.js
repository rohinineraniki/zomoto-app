import React, { useState } from "react";
import { useSelector } from "react-redux";
import { useLocation } from "react-router-dom";

function Cuisines(props) {
  const { search } = useLocation();
  const searchUrlParams = new URLSearchParams(search);

  const [cuisinesList, setCuisinesList] = useState([]);

  console.log("get from url", searchUrlParams.getAll("cuisines"));
  let prevCuisinesList = searchUrlParams.getAll("cuisines");

  const { getCuisinesList } = props;

  console.log("cuisinesList", cuisinesList);

  //getCuisinesList(cuisinesList);
  const cuisinesTypes = useSelector((state) => {
    return state.categories.cuisinesTypes;
  });

  const handleCuisinesChange = (event) => {
    console.log(event.target.checked);

    if (event.target.checked) {
      // setCuisinesList([

      //   ...cuisinesList,
      //   event.target.name,
      // ]);
      console.log("name", event.target.name);
      getCuisinesList(event.target.name, null);

      console.log("checkedCuisines in push", cuisinesList);
    } else {
      // let filteredCheckedCuisines = [...prevCuisinesList, ...cuisinesList];
      // filteredCheckedCuisines = filteredCheckedCuisines.filter((each) => {
      //   //console.log("each", each, event.target.name);
      //   return each !== event.target.name;
      // });
      // setCuisinesList(filteredCheckedCuisines);
      getCuisinesList(null, event.target.name);
      //console.log("remove", filteredCheckedCuisines, cuisinesList);
    }

    // setCuisinesList(cuisinesList);
    //getCuisinesList(cuisinesList);
  };

  const renderCuisines = (each) => {
    return (
      <div className="form-check">
        <input
          className="form-check-input"
          type="checkbox"
          id={each.toLowerCase().split(" ").join("")}
          name={each}
          defaultChecked={prevCuisinesList.includes(each)}
          onChange={handleCuisinesChange}
        />
        <label
          class="form-check-label mt-1"
          htmlFor={each.toLowerCase().split(" ").join("")}
        >
          {each}
        </label>
      </div>
    );
  };

  return (
    <div className="cuisines-container">
      <h5 className="mb-4">Cuisines</h5>
      <div className="d-inline-flex flex-column flex-wrap gap-2">
        {cuisinesTypes.map((each) => {
          return renderCuisines(each);
        })}
      </div>
    </div>
  );
}

export default Cuisines;
