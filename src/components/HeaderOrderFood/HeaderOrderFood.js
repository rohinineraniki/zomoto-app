import React, { useContext, useState } from "react";

import { SearchRestaurant } from "../OrderFood/OrderFood";
import zomatoLogo from "../../images/zomoto-footer.avif";
import { Link } from "react-router-dom";

import Login from "../Login/Login";
import Signup from "../Signup/Signup";
import "./HeaderOrderFood.css";
import { connect } from "react-redux";

function HeaderOrderFood(props) {
  const { searchValue, setSearchValue } = useContext(SearchRestaurant);
  const [logoutPopup, setLogoutPopup] = useState(false);

  const handleSearch = (event) => {
    setSearchValue(event.target.value);
  };

  const handleLogin = (event) => {
    if (event.target.textContent === "Login") {
      props.isLogin(true);
    } else if (event.target.textContent === "Logout") {
      setLogoutPopup(true);
    }
  };

  const handleNoLogout = () => {
    setLogoutPopup(false);
  };

  const handleYesLogout = () => {
    props.removeCart();
    props.loginUserDetails(undefined);
    setLogoutPopup(false);
  };

  const handleSignup = (event) => {
    if (event.target.textContent === "Signup") {
      props.isSignup(true);
    }
  };

  const removeSignup = (status, loginStatus) => {
    props.isSignup(status);
    if (loginStatus === true) {
      props.isLogin(loginStatus);
    }
  };

  const removeLogin = (status, loginUser) => {
    props.isLogin(status);

    if (loginUser === true) {
      props.isSignup(loginUser);
    } else {
      props.loginUserDetails(loginUser);
    }
  };

  return (
    <>
      <div className="online-order-header">
        <div className="online-header-container d-flex align-items-center justify-content-between m-3">
          <Link to="/">
            <img src={zomatoLogo} alt="zomato" className="logo-footer" />
          </Link>

          <div className="container-fluid w-50 d-none d-md-inline">
            <div className="row search-container d-flex flex-sm-column flex-md-row bg-white ps-2">
              <div className="col-12 col-md-4 d-flex align-items-center gap-2">
                <i
                  class="fa-solid fa-location-dot fa-lg"
                  style={{ color: "#e85f5f" }}
                ></i>
                <input
                  type="text"
                  placeholder="Location"
                  className="text-input"
                />
                <i
                  class="fa-solid fa-caret-down"
                  style={{ color: "#384251" }}
                ></i>
                <hr className="line align-self-end d-none d-md-inline" />
              </div>
              <div className="col-12 col-md-6 d-flex align-items-center gap-2  ">
                <i
                  class="fa-solid fa-magnifying-glass"
                  style={{ color: "#384251" }}
                ></i>
                <input
                  type="text"
                  placeholder="Search for restaurant"
                  className="text-input"
                  onChange={handleSearch}
                />
              </div>
            </div>
          </div>
          <div className="d-flex gap-3">
            <p onClick={handleLogin} className="login-btn">
              {props.loginUserName === undefined || props.loginUserName === ""
                ? "Login"
                : "Logout"}
            </p>
            <p onClick={handleSignup} className="login-btn">
              {props.loginUserName === undefined || props.loginUserName === ""
                ? "Signup"
                : props.loginUserName}
            </p>
          </div>
        </div>
        <div className="container-fluid w-100 d-md-none mt-4">
          <div className="row search-container d-flex flex-sm-column flex-md-row bg-white ps-2">
            <div className="col-5 d-flex align-items-center gap-2">
              <i
                class="fa-solid fa-location-dot fa-lg"
                style={{ color: "#e85f5f" }}
              ></i>
              <input
                type="text"
                placeholder="Location"
                className="text-input"
              />
              <i
                class="fa-solid fa-caret-down"
                style={{ color: "#384251" }}
              ></i>
              <hr className="line align-self-end d-none d-md-inline" />
            </div>
            <div className="col-7 d-flex align-items-center gap-2  ">
              <i
                class="fa-solid fa-magnifying-glass"
                style={{ color: "#384251" }}
              ></i>
              <input
                type="text"
                placeholder="Search for restaurant"
                className="text-input"
                onChange={handleSearch}
              />
            </div>
          </div>
        </div>
      </div>
      {props.login && <Login removeLogin={removeLogin} />}
      {props.signup && <Signup removeSignup={removeSignup} />}
      {logoutPopup && (
        <div className="bg-blur">
          <div className="signup-popup text-black d-flex flex-column align-items-center gap-2">
            <h6>Saved items in the cart will be discorded after logout</h6>
            <h5>Are you sure to logout?</h5>
            <div>
              <button className="btn btn-danger me-3" onClick={handleNoLogout}>
                No
              </button>
              <button className="btn btn-warning" onClick={handleYesLogout}>
                Yes
              </button>
            </div>
          </div>
        </div>
      )}
    </>
  );
}

const mapStateToProps = (state) => {
  return {
    login: state.categories.isLogin,
    signup: state.categories.isSignup,
    loginUserName: state.categories.loginUserName,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    isLogin: (status) => {
      dispatch({
        type: "USER_LOGIN",
        payload: { status },
      });
    },
    isSignup: (status) => {
      dispatch({
        type: "USER_SIGNUP",
        payload: { status },
      });
    },
    loginUserDetails: (loginUserName) => {
      dispatch({
        type: "LOGIN_USER_DETAILS",
        payload: { loginUserName },
      });
    },
    removeCart: () => {
      dispatch({
        type: "REMOVE_CART_ITEMS",
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(HeaderOrderFood);
