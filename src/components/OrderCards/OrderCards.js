import React from "react";
import { useNavigate } from "react-router-dom";

import HomeCard from "../HomeCard/HomeCard";
import orderimage from "../../images/main/order-online.avif";
import dining from "../../images/main/dining.avif";

import nightlife from "../../images/main/nightlife.avif";
import "./OrderCards.css";

function OrderCards() {
  let orderText = "Stay home and order to your doorstep";
  let diningText = "View the city's favourite dining venues";
  let nightText = "Explore the city's top nightlife outlets";
  return (
    <div className="container-fluid mt-4 d-flex justify-content-center">
      <div className="row d-flex order-cards-container">
        <HomeCard
          orderimage={orderimage}
          text="Order Online"
          textPara={orderText}
          urlPath="/order-food-online"
        />

        <HomeCard orderimage={dining} text="Dining" textPara={diningText} />
        <HomeCard
          orderimage={nightlife}
          text="Nightlife and clubs"
          textPara={nightText}
        />
      </div>
    </div>
  );
}

export default OrderCards;
