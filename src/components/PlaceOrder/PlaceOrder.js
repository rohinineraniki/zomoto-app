import React, { useState } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import notfoundImage from "../../images/notfound-image.avif";
import "./PlaceOrder.css";

function PlaceOrder(props) {
  let totalPrice = 0;
  let totalQuantity = 0;

  const itemsAddedToCart = props.restaurantsList.filter((each) => {
    const filteredDishes = each.dishes.filter((eachDish) => {
      totalPrice += eachDish.price * eachDish.quantity;

      totalQuantity += eachDish.quantity;
      return eachDish.quantity > 0;
    });

    if (filteredDishes.length !== 0) {
      return each;
    }
  });

  const displayOrderItems = (orderDish, restautantTitle) => {
    const { dishImage, dishName, id, price, quantity } = orderDish;

    return (
      <div className="col-12  d-flex gap-mb-4 gap-2 mb-3 single-dish-container mt-4">
        <div className="mb-3">
          <img
            src={dishImage}
            alt="dish"
            className="dish-order-image mt-1 me-2"
          />
        </div>
        <div className="mb-3 dish-title-container d-flex flex-column gap-3">
          <h4>{dishName}</h4>

          <div className="d-flex">
            <i
              class="fa-solid fa-indian-rupee-sign"
              style={{ color: "#3a3a3b" }}
            ></i>
            <p>{price * quantity}</p>
          </div>

          {quantity >= 1 ? (
            <div className="d-flex gap-4 quatity-container ">
              <p
                className="quatity-para"
                onClick={() => {
                  props.decrement(id, restautantTitle);
                }}
              >
                -
              </p>
              <p>{quantity}</p>
              <p
                className="quatity-para"
                onClick={() => {
                  props.increment(id, restautantTitle);
                }}
              >
                +
              </p>
            </div>
          ) : null}
        </div>
      </div>
    );
  };

  console.log("dishes", itemsAddedToCart);
  return (
    <div className="place-order-container">
      <div className="container-fluid d-flex justify-content-center">
        <div className="row  all-items-container">
          {itemsAddedToCart.length === 0 ? (
            <div className="d-flex justify-content-center ">
              <div className="not-found-container d-flex flex-column flex-md-row align-items-center ">
                <div>
                  <img
                    src={notfoundImage}
                    alt="Not-found"
                    className="not-found-image"
                  />
                </div>
                <div className="text-center">
                  <p>No items are added to cart</p>
                  <Link to="/">
                    <button className="btn btn-danger">Back to home</button>
                  </Link>
                </div>
              </div>
            </div>
          ) : (
            <>
              <div className="col-6">
                {itemsAddedToCart.map((each) => {
                  return each.dishes.map((orderDish) => {
                    if (orderDish.quantity > 0) {
                      return displayOrderItems(orderDish, each.title);
                    } else {
                      return null;
                    }
                  });
                })}
              </div>
              <div className="col-12 col-md-6 mt-4">
                <div className="total-price-container">
                  <div className="d-flex justify-content-between mb-3">
                    <h5>Subtotal</h5>
                    <div className="price-container d-flex align-items-center">
                      <i
                        class="fa-solid fa-indian-rupee-sign fa-xs mb-1"
                        style={{ color: "#141415" }}
                      ></i>
                      <p>{totalPrice}</p>
                    </div>
                  </div>
                  <div className="d-flex justify-content-between mb-2">
                    <p>GST and restaurant charges</p>
                    <div className="d-flex align-items-center">
                      <i
                        class="fa-solid fa-indian-rupee-sign fa-xs mb-1"
                        style={{ color: "#141415" }}
                      ></i>
                      <p>60</p>
                    </div>
                  </div>
                  <div className="d-flex justify-content-between">
                    <p>Delivery partner fee for 7 km</p>
                    <div className="d-flex align-items-center">
                      <i
                        class="fa-solid fa-indian-rupee-sign fa-xs mb-1"
                        style={{ color: "#141415" }}
                      ></i>
                      <p>50</p>
                    </div>
                  </div>
                  <hr />
                  <div className="d-flex justify-content-between">
                    <h6>Grand Total</h6>
                    <div className="d-flex align-items-center">
                      <i
                        class="fa-solid fa-indian-rupee-sign fa-xs"
                        style={{ color: "#141415" }}
                      ></i>
                      <p>{totalPrice + 110}</p>
                    </div>
                  </div>
                </div>
                <div className="mt-3">
                  {console.log(
                    "loginusername in placeorder",
                    props.loginUserName === ""
                  )}
                  {props.loginUserName !== undefined &&
                  props.loginUserName !== "" ? (
                    <Link to="/payment">
                      <button
                        className="btn btn-success"
                        onClick={() => {
                          props.orderPrice(totalPrice + 110);
                        }}
                      >
                        Place Order
                      </button>
                    </Link>
                  ) : (
                    <h5>Please login to place order</h5>
                  )}
                </div>
              </div>
            </>
          )}
        </div>
      </div>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    restaurantsList: state.categories.restaurants,
    loginUserName: state.categories.loginUserName,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    increment: (id, restautantTitle) => {
      dispatch({
        type: "INCREASE_QUANTITY",
        payload: { id, restautantTitle },
      });
    },
    decrement: (id, restautantTitle) => {
      dispatch({
        type: "DECREASE_QUANTITY",
        payload: { id, restautantTitle },
      });
    },
    orderPrice: (totalPrice) => {
      dispatch({
        type: "ORDER_PRICE",
        payload: { totalPrice },
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PlaceOrder);
