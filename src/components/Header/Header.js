import React from "react";
import Navbar from "../Navbar/Navbar";
import "./Header.css";
import logo from "../../images/zomato-logo.avif";
import zomoatoSmall from "../../images/zomato-small.avif";
import Search from "../Search/Search";

function Header() {
  return (
    <div className="container-fluid text-white w-100 header-container">
      <div className="row">
        <div className="header-bg-image d-flex flex-column header-container ">
          <Navbar />
          <div className="main-heading-container d-flex flex-column  align-items-center justify-content-center">
            <img src={logo} className="logo d-none d-md-inline" alt="logo" />
            <img
              src={zomoatoSmall}
              className="logo-sm d-md-none mt-3"
              alt="logo"
            />
            <h2 className="heading-para mt-4">
              Discover the best food & drinks
            </h2>
            <Search />
          </div>
        </div>
      </div>
    </div>
  );
}

export default Header;
