import React from "react";
import CollectionCard from "../CollectionCard/CollectionCard";

import winnerCo from "../../images/main/zomato-winner.avif";
import diningCo from "../../images/main/dining-co.avif";
import newCo from "../../images/main/new-co.avif";
import cafe from "../../images/main/cafe-co.avif";

import "./Collections.css";

function Collections() {
  let text1 = "Winners of Zomato Restaurant";
  let text2 = "Romantic Dining Places";
  let text3 = "Newly Opened Restaurants";
  let text4 = "Picturesque Cafes";
  return (
    <div className="mt-5 d-flex align-items-center justify-content-center collections-bg-container">
      <div className="collections-container ms-1">
        <h2 className="text-black heading-co">Collections</h2>
        <p className="para-co">
          Explore curated lists of top restaurants, cafes, pubs, and bars in
          Mumbai, based on trends
        </p>
        <div className="container-fluid text-white ">
          <div className="row d-flex justify-content-center align-items-center">
            <CollectionCard imageUrl={winnerCo} text={text1} count="18" />
            <CollectionCard imageUrl={diningCo} text={text2} count="13" />
            <CollectionCard imageUrl={newCo} text={text3} count="8" />
            <CollectionCard imageUrl={cafe} text={text4} count="9" />
          </div>
        </div>
      </div>
    </div>
  );
}

export default Collections;
