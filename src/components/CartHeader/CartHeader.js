import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import zomatoLogo from "../../images/zomoto-footer.avif";

import { connect } from "react-redux";
import Login from "../Login/Login";
import Signup from "../Signup/Signup";

import "./CartHeader.css";

function CartHeader(props) {
  const navigate = useNavigate();
  const [logoutPopup, setLogoutPopup] = useState(false);

  const getRestaurant = props.restaurantsList.filter((each) => {
    const filteredDishes = each.dishes.filter((eachDish) => {
      return eachDish.quantity > 0;
    });
    return filteredDishes.length > 0;
  });

  const getQuantity = () => {
    if (getRestaurant.length > 0) {
      const totalQuantity = getRestaurant[0].dishes.reduce((acc, each) => {
        return (acc += each.quantity);
      }, 0);
      return <span className="order-count">{totalQuantity}</span>;
    } else {
      return <span className="order-count">0</span>;
    }
  };

  const handleLogin = (event) => {
    if (event.target.textContent === "Login") {
      props.isLogin(true);
    } else if (event.target.textContent === "Logout") {
      setLogoutPopup(true);
      // props.loginUserDetails(undefined);
    }
  };

  const handelSignup = (event) => {
    if (event.target.textContent === "Signup") {
      props.isSignup(true);
    }
  };

  const removeLogin = (status, loginUserName) => {
    props.isLogin(status);
    if (loginUserName === true) {
      props.isSignup(loginUserName);
    } else {
      props.loginUserDetails(loginUserName);
    }
  };

  const removeSignup = (status, loginStatus) => {
    props.isSignup(status);
    if (loginStatus === true) {
      props.isLogin(loginStatus);
    }
  };

  const handleNoLogout = () => {
    setLogoutPopup(false);
  };

  const handleYesLogout = () => {
    props.loginUserDetails(undefined);
    props.removeCart();
    setLogoutPopup(false);
  };

  return (
    <>
      <div className="online-order-header d-flex align-items-center justify-content-around cart-header">
        <div className="online-header-container  m-3">
          <Link to="/">
            <img src={zomatoLogo} alt="zomato" className="logo-footer" />
          </Link>
        </div>
        <div className="d-flex align-items-center gap-3">
          <p onClick={handleLogin} className="login-btn">
            {props.loginUserName === undefined || props.loginUserName === ""
              ? "Login"
              : "Logout"}
          </p>
          <p onClick={handelSignup} className="login-btn">
            {props.loginUserName === undefined || props.loginUserName === ""
              ? "Signup"
              : props.loginUserName}
          </p>

          <button
            className="btn btn-success"
            onClick={() => {
              navigate("/view-cart");
            }}
          >
            Cart
            {getQuantity()}
          </button>
        </div>
      </div>
      {props.login && <Login removeLogin={removeLogin} />}
      {props.signup && <Signup removeSignup={removeSignup} />}
      {logoutPopup && (
        <div className="bg-blur">
          <div className="signup-popup text-black d-flex flex-column align-items-center gap-2">
            <h6>Saved items in the cart will be discorded after logout</h6>
            <h5>Are you sure to logout?</h5>
            <div>
              <button className="btn btn-danger me-3" onClick={handleNoLogout}>
                No
              </button>
              <button className="btn btn-warning" onClick={handleYesLogout}>
                Yes
              </button>
            </div>
          </div>
        </div>
      )}
    </>
  );
}

const mapStateToProps = (state) => {
  console.log("cart state", state);
  return {
    restaurantsList: state.categories.restaurants,
    orderItemsList: state.categories.cartItems.orderItemsList,

    loginUserName: state.categories.loginUserName,

    login: state.categories.isLogin,
    signup: state.categories.isSignup,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    loginUserDetails: (loginUserName) => {
      dispatch({
        type: "LOGIN_USER_DETAILS",
        payload: { loginUserName },
      });
    },
    isLogin: (status) => {
      console.log("status", status);
      dispatch({
        type: "USER_LOGIN",
        payload: { status },
      });
    },
    isSignup: (status) => {
      dispatch({
        type: "USER_SIGNUP",
        payload: { status },
      });
    },
    removeCart: () => {
      dispatch({
        type: "REMOVE_CART_ITEMS",
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CartHeader);
