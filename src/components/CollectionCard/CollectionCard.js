import React from "react";
import "./CollectionCard.css";

function CollectionCard({ imageUrl, text, count }) {
  return (
    <div className="col-6 col-md-3">
      <div
        className="collection-container d-flex  "
        style={{
          backgroundImage: `url(${imageUrl})`,
        }}
      >
        <div className="co-text align-self-end p-1">
          <p className="co-sub-heading">{text}</p>
          <div className="d-flex gap-2">
            <p>{count} Places</p>
            <i
              class="fa-solid fa-caret-down fa-rotate-270"
              style={{ color: "#ffffff" }}
            ></i>
          </div>
        </div>
      </div>
    </div>
  );
}

export default CollectionCard;
