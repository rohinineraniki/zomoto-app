import React, { useState } from "react";

import { connect } from "react-redux";
import debitCard from "../../images/payment/debit-card.png";
import paytm from "../../images/payment/paytm.png";
import phonepe from "../../images/payment/phonepay.png";
import upi from "../../images/payment/upi.png";
import mobikwik from "../../images/payment/mobikwik2.png";
import netBanking from "../../images/payment/net-banking.png";
import cod from "../../images/payment/casnondelivery.png";

import "./Payment.css";
import { useNavigate, Link } from "react-router-dom";

function Payment(props) {
  console.log(props.orderPrice);
  const [confirmPayment, setConfirmPayment] = useState(false);
  const [doPayment, setDoPayment] = useState(false);

  const handlePayment = () => {
    setConfirmPayment(true);
  };

  const handleNoPayment = () => {
    setConfirmPayment(false);
  };

  const handleYesPayment = () => {
    setConfirmPayment(false);
    setDoPayment(true);
    props.orderConfirm();
  };

  return (
    <>
      <div className="payment-container d-flex flex-column align-items-center py-3">
        <div className="d-flex my-4 align-items-center gap-4 each-payment-container">
          <Link to="/view-cart">
            <i
              className="fa-solid fa-arrow-right fa-rotate-180"
              style={{ color: "#222326" }}
            ></i>
          </Link>
          <h3>
            Bill total :
            <span className="ms-2">
              <i
                class="fa-solid fa-indian-rupee-sign fa-xs me-1"
                style={{ color: "#5a5b5e" }}
              ></i>
              {props.orderPrice}
            </span>
          </h3>
        </div>

        <div className="each-payment-container mb-3">
          <h5 className="mb-3 cards-heading ps-2">Cards</h5>

          <div className="d-flex align-items-center gap-3 ps-2">
            <img src={debitCard} alt="debitcard" className="payment-logo" />

            <p onClick={handlePayment} className="login-btn">
              Add Debit card
            </p>
          </div>

          <hr />
          <div className="d-flex align-items-center gap-3 ps-2">
            <img src={debitCard} alt="debitcard" className="payment-logo" />
            <p onClick={handlePayment} className="login-btn">
              Add Credit card
            </p>
          </div>
        </div>
        <div className="each-payment-container mb-3">
          <h5 className="cards-heading mb-3 ps-2">UPI</h5>

          <div className="d-flex align-items-center gap-3 ps-2">
            <img src={phonepe} alt="phonepe" className="payment-logo" />
            <p onClick={handlePayment} className="login-btn">
              PhonePe
            </p>
          </div>

          <hr />
          <div className="d-flex align-items-center gap-3 ps-2">
            <img src={paytm} alt="paytm" className="paytm-logo" />
            <p onClick={handlePayment} className="login-btn">
              Paytm
            </p>
          </div>

          <hr />
          <div className="d-flex align-items-center gap-3 ps-2">
            <img src={upi} alt="upi" className="payment-logo" />
            <p onClick={handlePayment} className="login-btn">
              Add new UPI ID
            </p>
          </div>
        </div>

        <div className="each-payment-container mb-3">
          <h5 className="cards-heading mb-3 ps-2">Wallets</h5>

          <div className="d-flex align-items-center gap-3 ps-2">
            <img src={paytm} alt="paytm" className="paytm-logo" />

            <div className="d-flex flex-column gap-1">
              <p onClick={handlePayment} className="login-btn">
                Paytm
              </p>
              <p className="wallet-text">Link your paytm wallet</p>
            </div>
          </div>

          <hr />
          <div className="d-flex align-items-center gap-3 ps-2">
            <img src={mobikwik} alt="mobikwik" className="mobikwik-logo" />

            <div className="d-flex flex-column gap-1">
              <p onClick={handlePayment} className="login-btn">
                Mobikwik
              </p>
              <p className="wallet-text">Link your Mobikwik wallet</p>
            </div>
          </div>
        </div>

        <div className="each-payment-container mb-3">
          <h5 className="cards-heading mb-3 ps-2"> Net Banking</h5>

          <div className="d-flex align-items-center gap-3 ps-2">
            <img src={netBanking} alt="netbanking" className="payment-logo" />
            <p onClick={handlePayment} className="login-btn">
              Netbanking
            </p>
          </div>
        </div>

        <div className="each-payment-container">
          <h5 className="cards-heading mb-3 ps-2"> Pay on Delivery</h5>

          <div className="d-flex align-items-center gap-3 ps-2">
            <img src={cod} alt="netbanking" className="payment-logo" />
            <p onClick={handlePayment} className="login-btn">
              Cash on delivery
            </p>
          </div>
        </div>
      </div>

      {confirmPayment && (
        <div className="bg-blur">
          <div className="confirm-payment-container d-flex flex-column align-items-center">
            <h4 className="mb-3">Confirm Payment</h4>

            <div className="d-flex gap-3">
              <button
                className="btn btn-danger login-btn"
                onClick={handleNoPayment}
              >
                No
              </button>
              <button
                className="btn btn-success login-btn"
                onClick={handleYesPayment}
              >
                Yes
              </button>
            </div>
          </div>
        </div>
      )}

      {doPayment && (
        <div className="bg-blur">
          <div className="confirm-payment-container d-flex flex-column align-items-center gap-3">
            <h4 className="mb-3">Your Order Confirmed !!</h4>
            <i
              class="fa-solid fa-circle-check fa-beat fa-2xl"
              style={{ color: "#10c61c" }}
            ></i>
            <Link to="/">
              <button className="btn btn-success mt-3">Home</button>
            </Link>
          </div>
        </div>
      )}
    </>
  );
}

const mapStateToProps = (state) => {
  return {
    orderPrice: state.categories.orderPrice,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    orderConfirm: () => {
      dispatch({
        type: "REMOVE_CART_ITEMS",
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Payment);
