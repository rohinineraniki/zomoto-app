import React from "react";
import "./Footer.css";
import logoFooter from "../../images/zomoto-footer.avif";
import indianFlag from "../../images/indiaflag.png";
import appleStore from "../../images/apple-store.webp";
import playstore from "../../images/playstore.webp";

function Footer() {
  return (
    <div className="container-fluid footer-container pb-4 d-flex justify-content-center">
      <div className="row d-flex justify-content-between footer-content">
        <div className="col-12 col-md-8 my-4">
          <img src={logoFooter} className="logo-footer" alt="logo" />
        </div>
        <div className=" col-12 col-md-4 my-4 d-flex ">
          <button className="btn btn-outline-secondary me-2">
            <img src={indianFlag} alt="flag" className="flag me-2" />
            India <span>&#709;</span>
          </button>
          <button className="btn btn-outline-secondary">
            <i
              class="fa-solid fa-globe fa-sm me-2"
              style={{ color: "#2f3032" }}
            ></i>
            English <span>&#709;</span>
          </button>
        </div>

        <ul className="col-6 col-md-2">
          <h6>ABOUT ZOMATO</h6>
          <li>Who We Are</li>
          <li>Blog</li>
          <li>Work With Us</li>
          <li>Investor Relations</li>
          <li>Report Fraud</li>
          <li>Contact Us</li>
        </ul>
        <ul className="col-6 col-md-2">
          <h6>ZOMAVERSE</h6>
          <li>Zomato</li>
          <li>Bilnkit</li>
          <li>Feeding India</li>
          <li>Hyperpure</li>
          <li>Zomaland</li>
        </ul>
        <div className="col-6 col-md-2">
          <ul>
            <h6>FOR RESTAURANT</h6>
            <li>Partner With Us</li>
            <li>Apps For You</li>
          </ul>
          <ul>
            <h6>FOR ENTERPRISES</h6>
            <li>Zomato For Enterprice</li>
          </ul>
        </div>
        <ul className="col-6 col-md-2">
          <h6>LEARN MORE</h6>
          <li>Privacy</li>
          <li>Security</li>
          <li>Terms</li>
          <li>Sitemap</li>
        </ul>
        <div className="col-12 d-md-none">
          <div className=" d-flex justify-content-around">
            <img src={appleStore} alt="apple-store" className="store" />
            <img src={playstore} alt="play-store" className="store" />
          </div>
        </div>
        <div className="col-12 col-md-2 d-flex flex-column align-items-center gap-2">
          <h6>SOCIAL LINKS</h6>
          <ul className="d-flex  gap-1">
            <li className="social-icon ">
              <i
                class="fa-brands fa-linkedin-in fa-xs"
                style={{ color: "#ffffff" }}
              ></i>
            </li>
            <li className="social-icon">
              <i
                class="fa-brands fa-instagram fa-xs mt-1"
                style={{ color: "#ffffff" }}
              ></i>
            </li>
            <li className="social-icon">
              <i
                class="fa-brands fa-twitter fa-xs mt-1"
                style={{ color: "#ffffff" }}
              ></i>
            </li>
            <li className="social-icon">
              <i
                class="fa-brands fa-youtube fa-xs mt-1"
                style={{ color: "#ffffff" }}
              ></i>
            </li>
            <li className="social-icon">
              <i
                class="fa-brands fa-facebook fa-xs mt-1"
                style={{ color: "#ffffff" }}
              ></i>
            </li>
          </ul>
          <div className="d-none d-md-block">
            <div className=" d-flex flex-column justify-content-around">
              <img src={appleStore} alt="apple-store" className="store" />
              <img src={playstore} alt="play-store" className="store" />
            </div>
          </div>
        </div>

        <hr />
        <div>
          <p className="rights-para">
            By continuing past this page, you agree to our Terms of service,
            Cookie Policy,Privacy Policy. All rights reserved.
          </p>
        </div>
      </div>
    </div>
  );
}

export default Footer;
