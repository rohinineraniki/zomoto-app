import React, { useState } from "react";
import validator from "validator";
import googleLogo from "../../images/header/google-logo.webp";
import { Link } from "react-router-dom";

function Login(props) {
  const { removeLogin } = props;

  const [validatePhone, isValidatePhone] = useState(false);
  const [phoneError, isPhoneError] = useState(false);
  const [nameError, isNameError] = useState(false);
  const [phoneErrorMsg, setPhoneErrorMsg] = useState("");

  const [phone, setPhone] = useState("");
  const [email, setEmail] = useState("");

  const [loginUserName, setLoginUserName] = useState("");
  const [signupError, setSignupError] = useState(false);

  const userSignup = JSON.parse(localStorage.getItem("userSignup"));

  const handelLoginSubmit = (event) => {
    event.preventDefault();

    if (userSignup === null) {
      setSignupError(true);
      isNameError(false);
    } else if (
      validator.isNumeric(phone) &&
      phone.length === 10 &&
      validator.isEmail(email) &&
      userSignup !== null
    ) {
      console.log("userSIgnup", userSignup);
      const checkUser = userSignup.filter((each) => {
        return each.email === email;
      });

      if (checkUser.length !== 0) {
        isPhoneError(false);
        isNameError(false);

        setSignupError(false);
        isValidatePhone(true);

        setLoginUserName(checkUser[0].name);
      } else {
        setLoginUserName("");
        isNameError(false);
        setSignupError(true);
      }
    } else if (phone.length < 10 || phone.length > 10) {
      setPhoneErrorMsg("Please enter 10 digit mobile number");
    } else {
      isPhoneError(true);
      isNameError(true);
    }
  };

  const handleCross = () => {
    removeLogin(false);
  };

  const handleHome = () => {
    isValidatePhone(false);

    removeLogin(false, loginUserName);
  };

  const handlePhoneBlur = (event) => {
    if (event.target.value === "") {
      isPhoneError(true);
    }
  };

  const handlePhoneChange = (event) => {
    setPhone(event.target.value);
  };

  const handleEmailBlur = (event) => {
    if (event.target.value === "") {
      isNameError(true);
    }
  };

  const handleCreateAccount = (event) => {
    removeLogin(false, true);
  };

  const handleEmailChange = (event) => {
    setEmail(event.target.value);
  };

  return (
    <>
      (
      <div className="bg-blur">
        <div className="signup-form-container text-black signup-popup py-4">
          <form onSubmit={handelLoginSubmit}>
            <div className="d-flex justify-content-between align-items-center mb-4">
              <h2>Login</h2>
              <i
                class="fa-solid fa-xmark fa-lg"
                style={{ color: "#2a2b2c" }}
                onClick={handleCross}
              ></i>
            </div>
            <div className="mb-3">
              <input
                type="number"
                placeholder="Phone"
                className="form-control"
                onBlur={handlePhoneBlur}
                onChange={handlePhoneChange}
                required
              />

              {phoneError && !validator.isNumeric(phone) && (
                <p className="error-msg">Please Enter Phone Number</p>
              )}

              {phone.length !== 10 && (
                <p className="error-msg">{phoneErrorMsg}</p>
              )}
            </div>
            <div className="mb-3">
              <input
                type="email"
                placeholder="Enter Email"
                className="form-control"
                onBlur={handleEmailBlur}
                onChange={handleEmailChange}
                required
              />

              {nameError && !validator.isEmail(email) && (
                <p className="error-msg">Invalid mail</p>
              )}
              {signupError && (
                <p className="error-msg">
                  Your details not found, Please Signup{" "}
                </p>
              )}
            </div>
            <button className="btn btn-danger w-100">Login</button>
          </form>
          <div className="my-3">
            <p className="text-black text-center">or</p>
            <button className="btn btn-outline-secondary w-100 mb-3">
              <i
                class="fa-solid fa-envelope fa-xl me-2"
                style={{ color: "#ef4e4e" }}
              ></i>
              Continue with Email
            </button>
            <button className="btn btn-outline-secondary w-100 mb-3">
              <img src={googleLogo} alt="logo" className="google-logo" />
              Continue with Google
            </button>
            <hr />
            <p>
              New to Zomato?{" "}
              <a
                href="#"
                className="terms-anchor"
                onClick={handleCreateAccount}
              >
                Create account
              </a>
            </p>
          </div>
        </div>
      </div>
      )
      {validatePhone && (
        <div className="bg-blur">
          <div className="success-popup text-success d-flex flex-column align-items-center gap-2">
            <h5>Login Successfully!!</h5>
            <Link to="/">
              <button className="btn btn-success" onClick={handleHome}>
                Home
              </button>
            </Link>
          </div>
        </div>
      )}
    </>
  );
}

export default Login;
