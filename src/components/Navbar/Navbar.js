import React, { useState } from "react";
import { connect } from "react-redux";

import "./Navbar.css";

import Login from "../Login/Login";
import Signup from "../Signup/Signup";

function Navbar(props) {
  console.log("props user", props.loginUser);
  const [logoutPopup, setLogoutPopup] = useState(false);

  const handleLogin = (event) => {
    if (event.target.textContent === "Login") {
      props.isLogin(true);
    } else if (event.target.textContent === "Logout") {
      setLogoutPopup(true);
    }
  };

  const handleNoLogout = () => {
    setLogoutPopup(false);
  };

  const handleYesLogout = () => {
    props.removeCart();
    props.loginUserDetails(undefined);
    setLogoutPopup(false);
  };

  const handleSignup = (event) => {
    if (event.target.textContent === "Signup") {
      props.isSignup(true);
    }
  };

  const removeSignup = (status, loginStatus) => {
    props.isSignup(status);
    if (loginStatus === true) {
      props.isLogin(loginStatus);
    }
  };

  const removeLogin = (status, loginUserName) => {
    props.isLogin(status);
    if (loginUserName === true) {
      props.isSignup(loginUserName);
    } else {
      props.loginUserDetails(loginUserName);
    }
  };

  return (
    <>
      <div className="d-flex flex-row justify-content-between navbar-container mt-3">
        <div className="d-flex align-items-center gap-1">
          <i class="fa-solid fa-mobile-screen-button fa-sm"></i>
          <p>Get the App</p>
        </div>
        <div className="d-none d-md-flex flex-row " style={{ gap: "3" + "em" }}>
          <h5>Investor Relations</h5>
          <h5>Add restaurant</h5>
          <h5 onClick={handleLogin} className="login-btn">
            {props.loginUser === undefined || props.loginUser === ""
              ? "Login"
              : "Logout"}
          </h5>
          <h5 onClick={handleSignup} className="login-btn">
            {props.loginUser === undefined || props.loginUser === ""
              ? "Signup"
              : props.loginUser}
          </h5>
        </div>
        <div className="d-md-none d-flex gap-2">
          <h5 onClick={handleLogin} className="login-btn">
            {props.loginUser === undefined || props.loginUser === ""
              ? "Login"
              : "Logout"}
          </h5>
          <h5 onClick={handleSignup} className="login-btn">
            {props.loginUser === undefined || props.loginUser === ""
              ? "Signup"
              : props.loginUser}
          </h5>
        </div>
      </div>
      {props.signup && <Signup removeSignup={removeSignup} />}
      {props.login && <Login removeLogin={removeLogin} />}
      {logoutPopup && (
        <div className="bg-blur">
          <div className="signup-popup text-black d-flex flex-column align-items-center gap-2">
            <h6>Saved items in the cart will be discorded after logout</h6>
            <h5>Are you sure to logout?</h5>
            <div>
              <button className="btn btn-danger me-3" onClick={handleNoLogout}>
                No
              </button>
              <button className="btn btn-warning" onClick={handleYesLogout}>
                Yes
              </button>
            </div>
          </div>
        </div>
      )}
    </>
  );
}

const mapStateToProps = (state) => {
  console.log("navbar cart", state);
  return {
    loginUser: state.categories.loginUserName,
    login: state.categories.isLogin,
    signup: state.categories.isSignup,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    loginUserDetails: (loginUserName) => {
      dispatch({
        type: "LOGIN_USER_DETAILS",
        payload: { loginUserName },
      });
    },
    isLogin: (status) => {
      dispatch({
        type: "USER_LOGIN",
        payload: { status },
      });
    },
    isSignup: (status) => {
      dispatch({
        type: "USER_SIGNUP",
        payload: { status },
      });
    },
    removeCart: () => {
      dispatch({
        type: "REMOVE_CART_ITEMS",
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Navbar);
