export const COST_LOW_TO_HIGH = "COST_LOW_TO_HIGH";
export const RATING_RANGE = "RATING_RANGE";
export const RATING_HIGH_TO_LOW = "RATING_HIGH_TO_LOW";
export const DELIVERY_TIME = "DELIVERY_TIME";
export const COST_HIGH_TO_LOW = "COST_HIGH_TO_LOW";
export const FILTER_CUISIENS = "FILTER_CUISIENS";
export const RESET = "RESET";
export const SEARCH_LIST = "SEARCH_LIST";
