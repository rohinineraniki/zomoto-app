import {
  COST_LOW_TO_HIGH,
  RATING_RANGE,
  RATING_HIGH_TO_LOW,
  DELIVERY_TIME,
  COST_HIGH_TO_LOW,
  FILTER_CUISIENS,
  RESET,
  SEARCH_LIST,
} from "./actionTypes";

const initialState = {
  categories: [
    {
      id: "1",
      foodCategoryImageUrl:
        "https://b.zmtcdn.com/data/o2_assets/d0bd7c9405ac87f6aa65e31fe55800941632716575.png",
      title: "Pizza",
    },
    {
      id: "2",
      foodCategoryImageUrl:
        "https://b.zmtcdn.com/data/dish_images/ccb7dc2ba2b054419f805da7f05704471634886169.png",
      title: "Burger",
    },
    {
      id: "3",
      foodCategoryImageUrl:
        "https://b.zmtcdn.com/data/dish_images/d19a31d42d5913ff129cafd7cec772f81639737697.png",
      title: "Biryani",
    },
    {
      id: "4",
      foodCategoryImageUrl:
        "https://b.zmtcdn.com/data/dish_images/197987b7ebcd1ee08f8c25ea4e77e20f1634731334.png",
      title: "Chicken",
    },
    {
      id: "5",
      foodCategoryImageUrl:
        "https://b.zmtcdn.com/data/o2_assets/e444ade83eb22360b6ca79e6e777955f1632716661.png",
      title: "Fried Rice",
    },
    {
      id: "6",
      foodCategoryImageUrl:
        "https://b.zmtcdn.com/data/dish_images/c2f22c42f7ba90d81440a88449f4e5891634806087.png",
      title: "Rolls",
    },
    {
      id: "7",
      foodCategoryImageUrl:
        "https://b.zmtcdn.com/data/o2_assets/8dc39742916ddc369ebeb91928391b931632716660.png",
      title: "Dosa",
    },
    {
      id: "8",
      foodCategoryImageUrl:
        "https://b.zmtcdn.com/data/dish_images/d9766dd91cd75416f4f65cf286ca84331634805483.png?fit=around|120:120&crop=120:120;*,*",
      title: "Idli",
    },
  ],
  restaurants: [
    {
      title: "The Ora Cafe",
      foodCategory: "North Indian, Beverages",
      rating: "4.0",
      deliveryTime: "25",
      price: "200",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/rh1kpotnhdlbfruwgn2c",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "Bhilwara, Sanjay Colony",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "Mirchi Restaurant",
      foodCategory: "North Indian",
      rating: "3.9",
      deliveryTime: "20",
      price: "300",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/povypukzcottzmhmy4fy",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "Bhilwara, Bhopal Ganj",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "Hot Pizza",
      foodCategory: "Italian, Pizzas",
      rating: "2.0",
      deliveryTime: "23",
      price: "200",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/onrussybboydf7hpidkr",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "Old Bhilwara, Old Bhilwara",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "Bholenath Pizza Centre",
      foodCategory: "Fast Food, Pizzas",
      rating: "4.2",
      deliveryTime: "35",
      price: "200",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/gyfr4kyppruesvoecmdl",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "Bhilwara, Bhopal Ganj",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "Kanha ji daal bati",
      foodCategory: "Indian",
      rating: "4.2",
      deliveryTime: "26",
      price: "300",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/xary7u2zjewtkwpnjyll",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "Old Bhilwara, Old Bhilwara",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "Asha Restaurant",
      foodCategory: "North Indian, South Indian, Chinese, Snacks",
      rating: "4.5",
      deliveryTime: "22",
      price: "200",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/vczeztxo1wpk9pqxiucq",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "Old Bhilwara, Shastri Nagar",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "Eating Seating",
      foodCategory: "Fast Food",
      rating: "3.9",
      deliveryTime: "22",
      price: "200",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/fn3rq0hstxf30n5wdrbv",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "Bhilwara, Bhopal Ganj",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "Mahalaxmi Bakers",
      foodCategory: "North Indian, Desserts",
      rating: "4.0",
      deliveryTime: "17",
      price: "200",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/r0q2nzue0zgjl8xd2bzv",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "Bhilwara, Bhopal Ganj",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "Sonu Patties",
      foodCategory: "American, Fast Food, Pizzas, Snacks, Beverages",
      rating: "3.6",
      deliveryTime: "21",
      price: "149",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/dmwpvksmmgezvmrdxrvm",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "Old Bhilwara, Old Bhilwara",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "Hangout",
      foodCategory:
        "Chinese, South Indian, North Indian, Continental, Fast Food",
      rating: "3.7",
      deliveryTime: "20",
      price: "300",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/l3clh7uogyqu8b4igg9r",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "Bhilwara, Bhopal Ganj",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "Angeethi Restaurant (Hotel Ashoka Residency)",
      foodCategory: "Desserts, Thai",
      rating: "3.1",
      deliveryTime: "24",
      price: "200",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/vejmtuy9txuwelm0xeba",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "Bhilwara, Shastri Nagar",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "Mateshwari Falhari Center",
      foodCategory: "Snacks, Fast Food",
      rating: "4.5",
      deliveryTime: "18",
      price: "100",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/xdhojkajxdugzfwi0wdc",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "Old Bhilwara, Shastri Nagar",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "Hotel Raza",
      foodCategory: "Mughlai",
      rating: "3.2",
      deliveryTime: "20",
      price: "200",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/bqmuzk3g1kqyadrqgcxb",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "Bhilwara, Old Bhilwara",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "Bikaner Family Restaurant",
      foodCategory: "North Indian",
      rating: "3.2",
      deliveryTime: "23",
      price: "220",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/jecyl9m4vuwbolvrorua",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "Old Bhilwara, Azad Nagar",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "City Hart momos",
      foodCategory: "Snacks, Fast Food",
      rating: "4.5",
      deliveryTime: "29",
      price: "200",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/aoiedyp0xbdjphnflmwt",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "Bhilwara, Bhopal Ganj",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "INOX",
      foodCategory: "Fast Food",
      rating: "4.5",
      deliveryTime: "24",
      price: "400",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/b1fv0tsksuat0z34pmrp",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "Bhilwara, Gandhi Nagar",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "SM Maggi n pasta",
      foodCategory: "Fast Food, Pastas",
      rating: "4.5",
      deliveryTime: "23",
      price: "200",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/txxyx0ehqvmznwavmjtz",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "BHILWARA, R K Colony",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "Jodhpur Sweets",
      foodCategory: "Desserts, Snacks",
      rating: "4.1",
      deliveryTime: "16",
      price: "300",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/nxmmra9fydoaqprkrtqv",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "Bhilwara, Bhopal Ganj",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "Chalta Firta Restaurant",
      foodCategory: "North Indian, Rajasthani",
      rating: "4.5",
      deliveryTime: "31",
      price: "200",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/xg0vcdztyvdxjbxkswsf",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "Old Bhilwara, Shastri Nagar",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "SS Chowmein & Manchurian",
      foodCategory: "North Indian",
      rating: "3.6",
      deliveryTime: "22",
      price: "100",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/wy7bjujpqeo9bxl8da8d",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "Bhilwara, Bhopal Ganj",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "Chawla Chicken",
      foodCategory: "Mughlai, Punjabi",
      rating: "3.3",
      deliveryTime: "24",
      price: "300",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/wexrils7tj2dkiifbfv0",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "Nehru Road, Sanjay Colony",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "Baskin Robbins",
      foodCategory: "Desserts",
      rating: "4.0",
      deliveryTime: "20",
      price: "250",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/l8u5sjmpqgrrt1wixdha",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "Bhilwara, Bhopal Ganj",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "Saffron The Family Restaurant",
      foodCategory: "North Indian",
      rating: "3.5",
      deliveryTime: "21",
      price: "300",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/p31bjtoghmc6amfytqs7",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "BHILWARA, RAJASTHAN, Bhopal Ganj",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "Aloo Bada Centre",
      foodCategory: "Snacks",
      rating: "4.5",
      deliveryTime: "25",
      price: "50",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/vacqsxxsu84u6w38fg74",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "BHILWARA, Shyam Nagar",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "Bikaner Bhojnalaya",
      foodCategory: "North Indian",
      rating: "4.5",
      deliveryTime: "32",
      price: "300",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/stmjaayn3x065rpdilgc",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "BHILWARA, Pur Road",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "O-zone Cafe",
      foodCategory: "Pizzas, Fast Food, Snacks, Chinese, Beverages",
      rating: "4.5",
      deliveryTime: "25",
      price: "100",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/vacqsxxsu84u6w38fg74",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "Bhilwara, Bhopal Ganj",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "Suruchi Restaurant",
      foodCategory: "North Indian",
      rating: "4.5",
      deliveryTime: "27",
      price: "300",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/nl2p1o5m6gvwvx0nfgck",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "BHILWARA, Ajmer Choraha",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "Crave Nation",
      foodCategory: "North Indian",
      rating: "4.5",
      deliveryTime: "22",
      price: "350",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/dqic0ecgabor01aggdwt",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "BHILWARA, Shubash Nager",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "The Brij Kanha restaurant",
      foodCategory: "Indian",
      rating: "4.5",
      deliveryTime: "27",
      price: "500",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/ju2jsn27fbqbpft50u6q",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "Bhilwara, Bhopal Ganj",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "The Roll Junction",
      foodCategory: "Fast Food, Mexican, North Indian",
      rating: "3.3",
      deliveryTime: "26",
      price: "250",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/bqbosireq9q5g5lv4eu6",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "BHILWARA, Bhagat Singh Colony",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "The Brown Sugar Bakery",
      foodCategory: "Desserts, Bakery",
      rating: "4.4",
      deliveryTime: "25",
      price: "300",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/fhupbdldqbjbb3bhq8ik",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "Bhilwara, Bhopal Ganj",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "Delhi Thali Restaurant",
      foodCategory: "North Indian, Desserts",
      rating: "3.7",
      deliveryTime: "19",
      price: "200",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/ss6esa4ktlcnbwy9xdje",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "Old Bhilwara, Old Bhilwara",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "Hotel Apsara",
      foodCategory: "North Indian",
      rating: "3.2",
      deliveryTime: "24",
      price: "200",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/ax3hkr2wr21qy7otxqss",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "Old Bhilwara, Shastri Nagar",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "Shree Sanwariya Restaurant",
      foodCategory: "North Indian, Rajasthani",
      rating: "4.2",
      deliveryTime: "27",
      price: "200",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/akbnfuabaonpdvrmagcb",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "Old Bhilwara, Azad Nagar",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "Score board sports cafe",
      foodCategory: "Fast Food, Italian, North Indian, Pizzas",
      rating: "3.1",
      deliveryTime: "25",
      price: "250",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/kswi0kmh2cicxl7a7fla",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "Bhilwara, Old Bhilwara",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "Mami ka pizza",
      foodCategory: "Indian",
      rating: "3.0",
      deliveryTime: "25",
      price: "250",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/utfuoomhsc1zqoaddl8e",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "Old Bhilwara, Azad Nagar",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "Apsara patties and pizza",
      foodCategory: "Pizzas, Fast Food, Beverages",
      rating: "4.5",
      deliveryTime: "24",
      price: "300",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/e1pqshqyzumuwmmgn0qo",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "Bhilwara, Gandhi Nagar",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "Harichhaya Garden restaurant",
      foodCategory: "North Indian",
      rating: "4.5",
      deliveryTime: "27",
      price: "200",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/aj8uo8zfarfme9jzhaci",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "Bhilwara, Bhopal Ganj",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "Roll express",
      foodCategory: "North Indian, Chinese, Snacks",
      rating: "4.1",
      deliveryTime: "27",
      price: "250",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/bj8dju5wf8h0wikjrr11",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "BHILWARA, R K Colony",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "Gourmet Ice Cream Cakes by Baskin Robbins",
      foodCategory: "Desserts, Ice Cream, Bakery",
      rating: "4.5",
      deliveryTime: "29",
      price: "250",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/py1nqakqmyioblax02yj",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "Nehru Road, Sanjay Colony",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "Bobbys cafe",
      foodCategory: "Fast Food",
      rating: "4.5",
      deliveryTime: "26",
      price: "300",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/qeio1yxsmrgda5en1p10",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "BHILWARA, RAJASTHAN, Bhopal Ganj",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "Eva s Eatery",
      foodCategory: "Fast Food, Beverages",
      rating: "4.5",
      deliveryTime: "25",
      price: "200",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/t20gtkpmkxusbr2ovzpg",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "Bhilwara, Bhopal Ganj",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "SN Pizza",
      foodCategory: "North Indian",
      rating: "4.5",
      deliveryTime: "31",
      price: "150",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/yrykqhbkcci76vuwcysw",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "BHILWARA, Shyam Nagar",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "The Storix cafe",
      foodCategory: "Italian, Continental, Beverages",
      rating: "4.5",
      deliveryTime: "30",
      price: "300",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/vgfv2xsmjvxbj4rizngu",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "BHILWARA, Pur Road",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "TEA & TALKS",
      foodCategory: "Beverages, Snacks, Pizzas",
      rating: "4.5",
      deliveryTime: "26",
      price: "200",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/rnupdiyo6o09n196kiib",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "BHILWARA, Ajmer Choraha",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "Shree Sanwariya Bhojnalaya",
      foodCategory: "Indian, Rajasthani",
      rating: "4.5",
      deliveryTime: "34",
      price: "200",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/cwfw3nw1coisgsu8k17n",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "Bhilwara, Bhopal Ganj",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "Sip & Gossip Cafe",
      foodCategory: "North Indian",
      rating: "3.9",
      deliveryTime: "31",
      price: "300",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/rgwsbhxhaf5ns57z29v1",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "BHILWARA, Shubash Nager",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "Ministry of Pastry Bakehouse",
      foodCategory: "Bakery, Desserts, Snacks, Beverages",
      rating: "4.5",
      deliveryTime: "25",
      price: "250",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/n0hq2htqutjec6fe2mjj",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "BHILWARA, Bhagat Singh Colony",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "Hotel Tulip continental",
      foodCategory:
        "Chinese, South Indian, North Indian, Continental, American, Desserts, Fast Food,...",
      rating: "4.5",
      deliveryTime: "31",
      price: "500",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/agrvbjh6y7ydi7ugp2ow",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "Bhilwara, Bhopal Ganj",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "The Mojo Cake N Bake",
      foodCategory: "Fast Food, Chinese, Beverages",
      rating: "4.5",
      deliveryTime: "33",
      price: "300",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/j6agxyn7x724sq7w6e2j",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "Old Bhilwara, Old Bhilwara",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "Coffee Culture",
      foodCategory:
        "Pizzas, Pastas, Italian, Indian, American, Chinese, Fast Food, Mexican, Beverage...",
      rating: "4.5",
      deliveryTime: "27",
      price: "350",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/rmldmdjbrtdn5ugjzi1c",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "Bhilwara, Bhopal Ganj",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "Blue Umbrella Bakers",
      foodCategory: "Bakery, Desserts",
      rating: "4.5",
      deliveryTime: "27",
      price: "400",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/qwpuyymmguzi5kfqxymv",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "Old Bhilwara, Shastri Nagar",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "Hari Sewa Bakers",
      foodCategory: "Bakery",
      rating: "4.5",
      deliveryTime: "33",
      price: "200",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/dfm62dmaras86pzn8mdr",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "Bhilwara, Old Bhilwara",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "Surprise India",
      foodCategory: "Bakery",
      rating: "4.5",
      deliveryTime: "32",
      price: "200",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/ew8npb9jay2qcbjll5ff",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "Old Bhilwara, Azad Nagar",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "Shreenath Pav Bahji",
      foodCategory: "North Indian, Fast Food",
      rating: "4.5",
      deliveryTime: "24",
      price: "100",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/ikihp6dkkwf1zgijj56f",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "Bhilwara, Bhopal Ganj",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "Balaji Fast Food",
      foodCategory: "Fast Food",
      rating: "4.5",
      deliveryTime: "32",
      price: "150",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/fcucdkclujxlwh7q3ntt",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "Bhilwara, Gandhi Nagar",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "Domino's Pizza",
      foodCategory: "Pizzas",
      rating: "3.5",
      deliveryTime: "30",
      price: "400",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/vomknfcgepzvg2nh6sr4",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "BHILWARA, R K Colony",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "Sukhwal Fast Food",
      foodCategory: "Fast Food",
      rating: "3.6",
      deliveryTime: "25",
      price: "200",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/ufk6hwdva4ygf4ywvddl",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "Nehru Road, Sanjay Colony",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "Seven Stone",
      foodCategory: "Continental, Fast Food, North Indian",
      rating: "4.5",
      deliveryTime: "34",
      price: "300",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/kuyej4p4605f9pz927m4",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "Bhilwara, Bhopal Ganj",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "Dev Restaurant",
      foodCategory: "North Indian",
      rating: "3.6",
      deliveryTime: "30",
      price: "200",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/dsckhsggcpekxosqih7w",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "BHILWARA, RAJASTHAN, Bhopal Ganj",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "Street Wok",
      foodCategory: "Fast Food",
      rating: "4.5",
      deliveryTime: "27",
      price: "200",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/ekowsaxassuzm779t451",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "BHILWARA, Shyam Nagar",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
    {
      title: "Love bird pizza",
      foodCategory: "Continental",
      rating: "4.0",
      deliveryTime: "25",
      price: "200",
      imageUrl:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/yrykqhbkcci76vuwcysw",
      offerStatus: true,
      offer: 60,
      couponCode: "STEALDEAL",

      address: "Bhilwara, Bhopal Ganj",
      dishes: [
        {
          id: "1",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "2",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "3",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "4",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "5",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
        {
          id: "6",
          quantity: 0,
          dishName: "eggtoast",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1525351484163-7529414344d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80",
          isBestSeller: true,
          rating: 4,
          reviews: 1200,
          price: 110,
          about:
            "Eggs provide a source of protein and fat, while toast is a source of complex carbohydrates. Together, this triple whammy offers good nutrient diversity, which is fundamental for a healthy breakfast that keeps you full for longer.",
        },
        {
          id: "7",
          quantity: 0,
          dishName: "Croissant",
          isVeg: false,
          dishImage:
            "https://images.unsplash.com/photo-1530610476181-d83430b64dcd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80",
          isBestSeller: false,
          rating: 3.5,
          reviews: 900,
          price: 150,
          about:
            "A croissant is a buttery, flaky, Austrian viennoiserie pastry inspired by the shape of the Austrian kipferl but using the French yeast-leavened laminated dough.",
        },
        {
          id: "8",
          quantity: 0,
          dishName: "salad",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1513442542250-854d436a73f2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=947&q=80",
          isBestSeller: true,
          rating: 3,
          reviews: 1700,
          price: 230,
          about:
            "A salad is a dish consisting of mixed, mostly natural ingredients with at least one raw ingredient. They are typically served at room temperature or chilled, though some can be served warm. Condiments and salad dressings, which exist in a variety of flavors, are often used to enhance a salad.",
        },
        {
          id: "9",
          quantity: 0,
          dishName: "pancake",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1506084868230-bb9d95c24759?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80",
          isBestSeller: true,
          rating: 5,
          reviews: 2200,
          price: 180,
          about:
            "A pancake is a flat cake, often thin and round, prepared from a starch-based batter that may contain eggs, milk and butter and cooked on a hot surface such as a griddle or frying pan, often frying with oil or butter. It is a type of batter bread.",
        },
        {
          id: "10",
          quantity: 0,
          dishName: "sandwitch",
          isVeg: true,
          dishImage:
            "https://images.unsplash.com/photo-1553909489-cd47e0907980?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80",
          isBestSeller: false,
          rating: 2,
          reviews: 3000,
          price: 200,
          about:
            "A sandwich is a food typically consisting of vegetables, sliced cheese or meat, placed on or between slices of bread, or more generally any dish wherein bread serves as a container or wrapper for another food type. The sandwich began as a portable, convenient finger food in the Western world, though over time it has become prevalent worldwide.",
        },
      ],
    },
  ],
  cartItems: { restaurantName: "", orderItemsList: [] },
  cuisinesTypes: [
    "Beverages",
    "Chinese",
    "Continental",
    "Desserts",
    "Fast Food",
    "Indian",
    "Italian",
    "North Indian",
    "Pizzas",
    "Snacks",
  ],
  loginUserName: "",
  orderPrice: "",
  isLogin: false,
  isSignup: false,
  isLogout: false,
  applyFilterList: [],
  ratingList: [],
};

export default function (state = initialState, action) {
  switch (action.type) {
    case "ADD_TO_CART":
      console.log("quantity", action.payload.quantity);
      if (
        state.cartItems.restaurantName === action.payload.restautantTitle ||
        state.cartItems.restaurantName === ""
      ) {
        return {
          ...state,

          restaurants: state.restaurants.map((each) => {
            if (each.title === action.payload.restautantTitle) {
              return {
                ...each,
                dishes: each.dishes.map((dish) => {
                  if (dish.id === action.payload.id) {
                    return {
                      ...dish,
                      quantity: dish.quantity + 1,
                    };
                  } else {
                    return dish;
                  }
                }),
              };
            } else {
              return each;
            }
          }),
          cartItems: {
            restaurantName: action.payload.restautantTitle,
            orderItemsList: [
              ...state.cartItems.orderItemsList,
              action.payload.foodItem,
            ],
          },
        };
      } else {
        return {
          ...state,
          restaurants: state.restaurants.map((each) => {
            if (each.title === state.cartItems.restaurantName) {
              return {
                ...each,
                dishes: each.dishes.map((dish) => {
                  return {
                    ...dish,
                    quantity: 0,
                  };
                }),
              };
            } else if (each.title === action.payload.restautantTitle) {
              return {
                ...each,
                dishes: each.dishes.map((dish) => {
                  if (dish.id === action.payload.id) {
                    return {
                      ...dish,
                      quantity: dish.quantity + 1,
                    };
                  } else {
                    return dish;
                  }
                }),
              };
            } else {
              return each;
            }
          }),
          cartItems: {
            restaurantName: action.payload.restautantTitle,
            orderItemsList: [action.payload.foodItem],
          },
        };
      }

    case "INCREASE_QUANTITY":
      return {
        ...state,
        restaurants: state.restaurants.map((each) => {
          if (each.title === action.payload.restautantTitle) {
            return {
              ...each,
              dishes: each.dishes.map((dish) => {
                if (dish.id === action.payload.id) {
                  return { ...dish, quantity: dish.quantity + 1 };
                } else {
                  return dish;
                }
              }),
            };
          } else {
            return each;
          }
        }),
      };
    case "DECREASE_QUANTITY":
      return {
        ...state,

        restaurants: state.restaurants.map((each) => {
          if (each.title === action.payload.restautantTitle) {
            return {
              ...each,
              dishes: each.dishes.map((dish) => {
                if (dish.id === action.payload.id) {
                  return { ...dish, quantity: dish.quantity - 1 };
                } else {
                  return dish;
                }
              }),
            };
          } else {
            return each;
          }
        }),
      };

    case "LOGIN_USER_DETAILS":
      return {
        ...state,
        loginUserName: action.payload.loginUserName,
      };

    case "ORDER_PRICE":
      return {
        ...state,
        orderPrice: action.payload.totalPrice,
      };

    case "USER_LOGIN":
      return {
        ...state,
        isLogin: action.payload.status,
      };

    case "USER_SIGNUP":
      return {
        ...state,
        isSignup: action.payload.status,
      };

    case "REMOVE_CART_ITEMS":
      return {
        ...state,
        restaurants: state.restaurants.map((each) => {
          return {
            ...each,
            dishes: each.dishes.map((eachDish) => {
              return { ...eachDish, quantity: 0 };
            }),
          };
        }),
      };
    case COST_LOW_TO_HIGH:
      const compareFn = (restaurant1, restaurant2) => {
        let restaurant1Price = Number(restaurant1.price);
        let restaurant2Price = Number(restaurant2.price);

        if (restaurant1Price > restaurant2Price) {
          return 1;
        }

        if (restaurant1Price < restaurant2Price) {
          return -1;
        }

        if (restaurant1Price === restaurant2Price) {
          return 0;
        }
      };

      const newRestaurantObj = [...state.applyFilterList];
      return {
        ...state,
        applyFilterList: newRestaurantObj.sort(compareFn),
      };

    case COST_HIGH_TO_LOW:
      const compareCostFn = (restaurant1, restaurant2) => {
        let restaurant1Price = Number(restaurant1.price);
        let restaurant2Price = Number(restaurant2.price);

        if (restaurant1Price > restaurant2Price) {
          return -1;
        }

        if (restaurant1Price < restaurant2Price) {
          return 1;
        }

        if (restaurant1Price === restaurant2Price) {
          return 0;
        }
      };

      const newRestaurantCostObj = [...state.applyFilterList];
      return {
        ...state,
        applyFilterList: newRestaurantCostObj.sort(compareCostFn),
      };

    case RATING_RANGE:
      console.log("in payload rating", action.payload.ratingValue);
      console.log("applyfilterlist rating", state.applyFilterList);

      const filteredRatingList = state.applyFilterList.filter((each) => {
        return Number(each.rating) >= Number(action.payload.ratingValue);
      });
      console.log("filteredRatingList", filteredRatingList);

      return {
        ...state,
        applyFilterList: filteredRatingList,
      };

    case RATING_HIGH_TO_LOW:
      console.log("in rating htl");
      const compareRatingFn = (item1, item2) => {
        let itemRating1 = Number(item1.rating);
        let itemRating2 = Number(item2.rating);

        if (itemRating1 > itemRating2) {
          return -1;
        }
        if (itemRating1 < itemRating2) {
          return 1;
        }
        if (itemRating1 === itemRating2) {
          return 0;
        }
      };

      const newSortRatingObj = [...state.applyFilterList];

      return {
        ...state,
        applyFilterList: newSortRatingObj.sort(compareRatingFn),
      };

    case DELIVERY_TIME:
      const compareSortDeliveryTime = (delivery1, delivery2) => {
        let deliveryTime1 = Number(delivery1.deliveryTime);
        let deliveryTime2 = Number(delivery2.deliveryTime);

        if (deliveryTime1 > deliveryTime2) {
          return 1;
        }
        if (deliveryTime1 < deliveryTime2) {
          return -1;
        }

        if (deliveryTime1 === deliveryTime2) {
          return 0;
        }
      };

      const newSortDeliveryTimeObj = [...state.applyFilterList];

      return {
        ...state,
        applyFilterList: newSortDeliveryTimeObj.sort(compareSortDeliveryTime),
      };

    case FILTER_CUISIENS:
      console.log("payload cuisines", action.payload.getAllCuisinesFromUrl);
      return {
        ...state,
        applyFilterList: state.applyFilterList.filter((each) => {
          let foodCategoryList = each.foodCategory.split(", ");
          let cuisinesList = action.payload.getAllCuisinesFromUrl;

          let addedList = [...foodCategoryList, ...cuisinesList];
          let addedListSet = new Set(addedList);

          return addedListSet.size !== addedList.length;
        }),
      };

    case RESET:
      return {
        ...state,
        applyFilterList: state.restaurants,
      };

    case SEARCH_LIST:
      let filteredSearchList = state.applyFilterList.filter((each) => {
        return each.title
          .toLowerCase()
          .includes(action.payload.searchValue.toLowerCase());
      });
      console.log("filteredSearchList", filteredSearchList);

      return {
        ...state,
        applyFilterList: filteredSearchList,
      };

    default:
      return { ...state, applyFilterList: state.restaurants };
  }
}
